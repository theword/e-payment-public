import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../common/authentication.service';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-news-detail',
    templateUrl: 'news-detail.component.html'
})

export class NewsDetailComponent implements OnInit, AfterViewInit {
    ngAfterViewInit(): void {
        jQuery(".responsive-calendar").responsiveCalendar({
            time: '2018-01',
            // events: {
            //     "2018-01-01": { "number": 5, "url": "http://w3widgets.com/responsive-slider" },
            //     "2018-01-26": { "number": 1, "url": "http://w3widgets.com/responsive-slider" },
            //     "2018-01-12": { "number": 1, "url": "http://w3widgets.com/responsive-slider" }
            // }
        });

    }
    news: any = {};
    images: any[] = [];
    imagesRaw: any[] = [];

    constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private auth: AuthenticationService) {
        this.route.params.subscribe(p => {
            this.http.get(`api/posts/${p.id}/withcount`).subscribe(r => {
                this.news = r;
                this.images = (this.news.meta as any[]).filter(i => i.key == "attach_fileinfo_id");

                if (this.news.status == 'Draft') {
                    for (var i = 0; i < this.news.meta.length; i++) {

                        if (this.news.meta[i].key == 'attach_fileinfo_id') {
                            this.readFileFromURL(this.news.meta[i], i).subscribe(r => {
                                this.imagesRaw.push(r);
                            });
                        }
                    }
                }

            });
        });
    }

    ngOnInit() { }
    getType(post) {
        if (!post.meta)
            return 0;

        let news_type = (post.meta as any[]).find(i => i.key === 'news_type');
        return news_type ? news_type.value : 0;

    }

    getThumbnail(post) {
        if (!post.meta)
            return null;
        let img = (post.meta as any[]).find(i => i.key == "attach_fileinfo_id");
        console.log(img);
        return img ? `files/posts/${img.id}.png` : undefined;
    }

    cancel() {
        this.router.navigateByUrl('/admin/news');
    }

    save() {
        var req = this.news;

        //   req.status = 'Published';
        req.type = "1";
        req.images = this.imagesRaw;
        this.auth.post(`api/cms/posts/${this.news.id}`, req).subscribe(v => {
            this.router.navigateByUrl('/admin/news');
        });


    }

    readFileFromURL(meta, key): Observable<any> {
        return Observable.create(ob => {
            var request = new XMLHttpRequest();
            request.open('GET', `files/posts/${meta.id}.png`, true);

            request.responseType = 'blob';
            request.onload = function () {
                var reader = new FileReader();
                reader.readAsDataURL(request.response);
                reader.onload = function (e: any) {
                    var base64 = e.target.result;
                    ob.next({
                        key: key,
                        data: base64,
                    })
                };
            };
            request.send();
        });
    }

}
