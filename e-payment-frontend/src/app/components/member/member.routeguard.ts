import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { environment } from '../../../environments/environment';
import { AuthenticationService } from '../common/authentication.service';

@Injectable()
export class MemberRouteGuard implements CanActivate {

    constructor(private router: Router, private auth: AuthenticationService) { }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {

        var observer = this.auth.get('api/member/validate')
            .map(r => r != null);
        observer.subscribe(m => {
            console.log(m);
        }, (er) => {
            this.router.navigateByUrl('/');
        });

        return observer;

    }


}
