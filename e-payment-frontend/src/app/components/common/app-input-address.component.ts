import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { EventEmitter } from '@angular/core';
@Component({
    selector: 'app-input-address',
    templateUrl: './app-input-address.component.html'
})
export class InputAddressComponent implements OnInit {

    @Input() forRegister = false;
    @Input() isDisabled = false;
    @Input() showRoad = true;
    provinces: Observable<any[]>;
    districts: Observable<any[]>;
    subdistricts: any[];

    address: FormGroup;

    constructor(private fb: FormBuilder, private http: HttpClient) {
        this.address = this.fb.group({
            id: new FormControl(null),
            no: new FormControl(null),
            moo: new FormControl(null),
            soi: new FormControl(null),
            road: new FormControl(null),
            provinceId: new FormControl(null),
            amphurId: new FormControl(null),
            tambonId: new FormControl(null),
            postCode: new FormControl(null, [Validators.minLength(5), Validators.maxLength(5)]),
            roomNo: new FormControl(null),
            village: new FormControl(null),
            provinceName: new FormControl(null),
            amphurName: new FormControl(null),
            tambonName: new FormControl(null),
            building_name: new FormControl(null),
            floor: new FormControl(null),
            status: new FormControl(null),
            userId: new FormControl(null),
            addressType: new FormControl(null),
            lane: new FormControl(null)
        });

        this.http.get('api/common/provinces').subscribe((r: any) => {
            this.provinces = r;
        });

        this.address.get('provinceId').valueChanges.subscribe(v => {
            if (v) {
                this.http.get('api/common/provinces/' + v + '/districts').subscribe((r: any) => {
                    this.districts = r;
                });
            } else {
                this.districts = null;
                this.address.get('amphurId').setValue(null);
            }
        });

        this.address.get('amphurId').valueChanges.subscribe(v => {
            if (v) {
                this.http.get('api/common/districts/' + v + '/subdistricts').subscribe((r: any) => {
                    this.subdistricts = r;
                });
            } else {
                this.subdistricts = null;
                this.address.get('tambonId').setValue(null);
            }
        });

        this.address.get('tambonId').valueChanges.subscribe(v => {
            if (v && this.subdistricts) {
                const subdistrict = this.subdistricts.find(s => s.id == v);
                this.address.get('postCode').setValue(subdistrict.postCode);
            }
        });

        this.address.valueChanges.subscribe(v => {

        });
    }

    ngOnInit() {
        if (!this.isDisabled) {
            this.address.get('no').setValidators([Validators.required]);
            this.address.get('provinceId').setValidators([Validators.required]);
            this.address.get('amphurId').setValidators([Validators.required]);
            this.address.get('tambonId').setValidators([Validators.required]);
            this.address.get('postCode').setValidators([Validators.required, Validators.minLength(5), Validators.maxLength(5)]);
        } else {
            this.address.get('provinceId').disable();
            this.address.get('amphurId').disable();
            this.address.get('tambonId').disable();
        }
    }

    set value(val) {
        if (!val)
            return;
        this.address.setValue({
            id: val.id,
            no: val.no,
            moo: val.moo,
            soi: val.soi,
            road: val.road,
            provinceId: val.provinceId,
            amphurId: val.amphurId,
            tambonId: val.tambonId,
            postCode: val.postCode,
            roomNo: val.roomNo,
            village: val.village,
            provinceName: val.provinceName,
            amphurName: val.amphurName,
            tambonName: val.tambonName,
            building_name: val.building_name,
            floor: val.floor,
            status: val.status,
            userId: val.userId,
            addressType: val.addressType,
            lane: val.lane,
        });
    }
}
