import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';
import * as faker from 'faker';
import { CaptchaComponent } from '../common/captcha.component';
import { ValidationService } from '../common/validation.service';
import { Router } from '@angular/router';
import { InputAddressComponent } from '../common/app-input-address.component';
import { DialogComponent } from '../common/app-dialog.component';

@Component({
    selector: 'app-register-corporate',
    templateUrl: 'register-corporate.component.html'
})

export class RegisterCorporateComponent implements OnInit, AfterViewInit {

    registerForm: FormGroup;

    agreement :any;
    titles: any[];
    inProgress: boolean;
    isRegisterComplete: boolean;
    isLoad: boolean;
    @ViewChild('modalOTPRegister') modalOTPRegister;
    @ViewChild('modalRegisterComplete') modalRegisterComplete;
    @ViewChild(CaptchaComponent) recaptchaRef: CaptchaComponent;

    @ViewChild('billingAddress') billingAddress: InputAddressComponent;
    @ViewChild('workingAddress') workingAddress: InputAddressComponent;
    @ViewChild('dialog') dialog: DialogComponent;

    titleMessage: any;
    message: any;
    constructor(private http: HttpClient, private fb: FormBuilder, private vs: ValidationService, private router: Router) {

    }

    createForm() {

        this.workingAddress.address.disable();

        this.registerForm = this.fb.group({
            companyId: new FormControl(null, [Validators.required]),
            companyNameTH: new FormControl(null, [Validators.required]),
            companyNameEN: new FormControl(null, [Validators.required]),
            companyStatus: new FormControl(null, [Validators.required]),
            companyObjective: new FormControl({ value: null, disabled: true }),
            company_auth_id: new FormControl(null, [Validators.required, this.vs.validatePersonNoFormat]),
            birthDate: new FormControl(null, [Validators.required]),
            titleNameTH: new FormControl(null, [Validators.required]),
            fnameTH: new FormControl(null),
            lnameTH: new FormControl(null),
            mnameTH: new FormControl(null),
            titleNameEN: new FormControl(null),
            fnameEN: new FormControl(null),
            lnameEN: new FormControl(null),
            mnameEN: new FormControl(null),

            company_auth_titleNameTH: new FormControl(null, [Validators.required]),
            company_auth_fnameTH: new FormControl(null, [Validators.required]),
            company_auth_lnameTH: new FormControl(null, [Validators.required]),
            company_auth_mnamTH: new FormControl(null),
            company_auth_titleNameEN: new FormControl(null),
            company_auth_fnameEN: new FormControl(null),
            company_auth_lnameEN: new FormControl(null),
            company_auth_mnameEN: new FormControl(),

            email: new FormControl(null, [Validators.required]),
            mobile: new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
            verifiedType: new FormControl('1', [Validators.required]),
            captcha: new FormControl(null, [Validators.required]),
            agreement: new FormControl(null, [Validators.required]),
            password: new FormControl(null, [Validators.required]),
            confirmPassword: new FormControl(null, [Validators.required]),

            billingAddress: this.billingAddress.address,
            workingAddress: this.workingAddress.address,


            file: new FormControl(null, [Validators.required]),
            statusText: new FormControl()
        }, { validator: [this.vs.passwordConfirming, this.vs.validatePassword] });

        this.registerForm.valueChanges.subscribe(v => {
            console.log(v);
        });

    }

    ngOnInit() {
        this.createForm();
        this.http.get('api/common/titles').subscribe((r: any) => {
            this.titles = r;
            this.registerForm.controls['titleNameTH'].setValue(r[0].id);
            this.registerForm.controls['titleNameEN'].setValue(r[0].id);
        });


        this.http.get('api/posts/downloads/single/agreement').map((o: any) => {
            return {
                postId: o.id,
                content: o.content,
                fileName: (o.meta as any[]).find(i => i.key === 'ATTACH_NAME'),
                downloaded: (o.meta as any[]).find(i => i.key === 'page_view'),
                length: (o.meta as any[]).find(i => i.key === 'ATTACH_LENGTH'),
                fileId: (o.meta as any[]).find(i => i.key === 'attach_fileinfo_id'),
                createdDate: o.createdDate
            };
        }).subscribe(v => {
            this.agreement = v;
        });
    }

    ngAfterViewInit() {

    }

    getCorporate() {
        this.isLoad = true;
        this.http.get(`api/member/corporate/${this.registerForm.controls['companyId'].value}`).subscribe((r: any[]) => {
            if (r.length > 0) {
                const businessInfo = r[0];

                if (businessInfo.status === '1') {
                    this.registerForm.controls['statusText'].setValue('ดำเนินกิจการ');
                } else if (businessInfo.status === '2') {
                    this.registerForm.controls['statusText'].setValue('ยกเลิกกิจการ');
                } else {
                    this.registerForm.controls['statusText'].setValue('อื่นๆ');
                }




                this.registerForm.controls['companyNameTH'].setValue(businessInfo.companyName);
                this.registerForm.controls['companyNameEN'].setValue(businessInfo.companyName);
                this.registerForm.controls['companyStatus'].setValue(businessInfo.status);
                this.registerForm.controls['companyObjective'].setValue(businessInfo.objectiveDesc);

                if (businessInfo.status !== '1') {
                    this.showDialog2('แจ้งข้อผิดพลาด', 'ท่านไม่สามารถลงทะเบียนได้ เนื่องจากนิติบุคคลนี้ไม่อยู่ในสถานะ "ดำเนินกิจการ" ');
                    this.isLoad = false;
                    return;
                }


                this.workingAddress.address.patchValue({
                    no: businessInfo.no,
                    village: businessInfo.village,
                    moo: businessInfo.moo,
                    soi: businessInfo.soi,
                    road: businessInfo.road,
                    postCode: businessInfo.postCode
                });



                this.registerForm.controls['companyObjective'].setValue(businessInfo.objectiveDesc);




                this.http.get('api/common/provinces').subscribe((p: any[]) => {

                    const province = p.find(x => x.nameTH === businessInfo.provinceName);
                    this.workingAddress.address.controls['provinceId'].setValue(province.id);
                    this.http.get('api/common/provinces/' + this.workingAddress.address.controls['provinceId'].value + '/districts')
                        .subscribe((d: any[]) => {

                            businessInfo.amphurName = businessInfo.amphurName.replace('เขต', '');
                            businessInfo.amphurName = businessInfo.amphurName.replace('อำเภอ', '');
                            const district = d.find(x => x.nameTH === businessInfo.amphurName);
                            this.workingAddress.address.controls['amphurId'].setValue(district.id);
                            this.http.get('api/common/districts/' + this.workingAddress.address.controls['amphurId'].value + '/subdistricts')
                                .subscribe((s: any[]) => {

                                    businessInfo.tambonName = businessInfo.tambonName.replace('ตำบล', '');
                                    businessInfo.tambonName = businessInfo.tambonName.replace('แขวง', '');
                                    const subdistrict = s.find(x => x.nameTH === businessInfo.tambonName);
                                    this.workingAddress.address.controls['tambonId'].setValue(subdistrict.id);
                                    this.workingAddress.address.controls['postCode'].setValue(subdistrict.postCode);
                                });
                        });

                });

            } else {
                alert('ไม่พบข้อมูล');
                this.registerForm.controls['companyNameTH'].reset();
                this.registerForm.controls['companyNameEN'].reset();
                this.registerForm.controls['companyStatus'].reset();
                this.registerForm.controls['companyObjective'].reset();
                this.workingAddress.address.reset();
            }
            this.isLoad = false;
        });
    }

    onSubmit() {
        this.inProgress = true;
        this.http.post('api/register/corporate', this.registerForm.getRawValue()).subscribe((r: any) => {
            this.showDialog(true);
            this.inProgress = false;
        }, (err: any) => {
            this.showDialog(false);
            this.inProgress = false;
        });
    }

    showDialog(registerComplete: boolean) {
        this.isRegisterComplete = registerComplete;
        if (this.registerForm.get('verifiedType').value === '1' && registerComplete) {
            this.showOTPDialog();
        } else {
            this.showRegisterDialog();
        }
    }

    showOTPDialog() {
        jQuery(this.modalOTPRegister.nativeElement).modal().show().on('hidden.bs.modal', () => {
            if (this.isRegisterComplete) {
                this.router.navigate(['/home']);
            } else {
                this.recaptchaRef.reset();
            }
        });
    }
    showRegisterDialog() {
        jQuery(this.modalRegisterComplete.nativeElement).modal().show().on('hidden.bs.modal', () => {
            if (this.isRegisterComplete) {
                this.router.navigate(['/home']);
            } else {
                this.recaptchaRef.reset();
            }
        });
    }

    revert() {
        this.registerForm.reset();
    }

    copyAddress(event, source, destination): void {
        if (event.target.checked) {
            let v = source.value;

            if (destination.value.id == null)
                v.id = null;
            else
                v.id = destination.value.id;


            destination.setValue(v);
        }

    }

    showDialog2(titleMessage, message): Observable<any> {
        this.titleMessage = titleMessage;
        this.message = message;
        const comp = this;
        return this.dialog.open();
    }

}



