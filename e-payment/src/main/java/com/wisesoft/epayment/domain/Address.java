package com.wisesoft.epayment.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {

    private String id;
    private String no;
    private String village;
    private String moo;
    private String lane;
    private String soi;
    private String provinceId;
    private String amphurId;
    private String tambonId;
    private String road;
    private String roomNo;
    private String provinceName;
    private String amphurName;
    private String tambonName;
    private String building_name;
    private String floor;
    private String postCode;
    private String status;
    private String userId;
    private String addressType;


}