import { Injectable } from '@angular/core';
import { FormControl, AbstractControl } from '@angular/forms/src/model';

@Injectable()
export class ValidationService {



    validatePassword(c: AbstractControl) {

        const minlength = 10;
        const pattern = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{0,}$/);
        const firstName = c.get('fnameEN').value;
        const lastName = c.get('lnameEN').value;

        if (c.get('password').value === null) {
            return;
        }
        if (c.get('password').value.length < minlength) {
            return { minlength: minlength };
        }

        const result = pattern.test(c.get('password').value);
        if (!result) {
            return { pattern: true };
        }

        if (!firstName || !lastName) {
            return;
        }

        if (c.get('password').value.includes(firstName.substring(0, 3)) || c.get('password').value.includes(lastName.substring(0, 3))) {
            return { passwordContainName: true };
        }
    }

    passwordConfirming(c: AbstractControl): { passwordMismatch: boolean } {
        if (c.get('password').value !== c.get('confirmPassword').value) {
            return { passwordMismatch: true };
        }
    }

    validatePersonNoFormat(control: AbstractControl) {
        if (!control.value) {
            return null;
        }
        let valid = true;

        if (control.value.toString().length === 13) {
            const id = control.value.toString();
            let sum = 0;

            for (let i = 0; i < 12; i++) {
                sum += parseFloat(id.charAt(i)) * (13 - i);
            }

            if ((11 - sum % 11) % 10 !== parseFloat(id.charAt(12))) {
                valid = false;
            }

        } else if (control.value.toString().length > 13) {
            return { personWrongFormat: 'เลขประจำตัวประชาชนห้ามเกิน 13 หลัก' };
        }
        return valid && control.value.toString().length === 13 ? null : { personWrongFormat: 'เลขบัตรประชาชนไม่ถูกต้อง' };

    }


}
