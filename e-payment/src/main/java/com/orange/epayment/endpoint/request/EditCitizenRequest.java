package com.orange.epayment.endpoint.request;

import lombok.Data;

@Data
public class EditCitizenRequest {
    
     private String birthDate;
     private String titleNameTH ;
     private String fnameTH;
     private String lnameTH; 
     private String mnameTH;
     private String titleNameEN;
     private String fnameEN; 
     private String lnameEN; 
     private String mnameEN; 
     private String email; 
     private String mobile;
     private String captcha;
     private String age;
     private Address address1;
     private Address address2;
     private Address address3;
}