package com.orange.epayment.data;

import org.springframework.data.repository.CrudRepository;

import com.orange.epayment.domain.File;

public interface FileRepository extends CrudRepository<File, Long> {

}
