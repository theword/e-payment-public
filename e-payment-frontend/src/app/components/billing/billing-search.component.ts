import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { CaptchaComponent } from '../common/captcha.component';
import { SuperForm } from 'angular-super-validator';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { UrlSegment } from '@angular/router';

@Component({
    selector: 'app-billing-search',
    templateUrl: './billing-search.component.html'
})
export class BillingSearchComponent implements OnInit {

    test = 1111222333;
    rows: any;
    errors: any;
    form: FormGroup;
    loading = false;
    env: any;

    @ViewChild('captcha') captcha: CaptchaComponent;
    constructor(private http: HttpClient) {
        this.errors = {};
    }

    ngOnInit() {
        this.form = new FormGroup({
            type: new FormControl(1),
            value: new FormControl(null, [Validators.required, Validators.minLength(14), Validators.maxLength(14)]),
            value2: new FormControl(null, [Validators.required, Validators.minLength(16), Validators.maxLength(16)]),
            captcha: new FormControl(null, Validators.required)
        });

        this.form.get('type').valueChanges.subscribe(v => {
            this.form.get('value').reset();
            this.form.get('value2').reset();
            this.errors = {};
        });

    }

    clear() {
        this.errors = {};
        this.form.patchValue({ type: 1 });
        this.captcha.reset();
        this.rows = undefined;
    }
    submit() {
        this.errors = SuperForm.getAllErrors(this.form);

        if (this.form.valid) {
            const search = this.form.getRawValue();
            this.rows = undefined;
            this.loading = true;

            const $env = this.http.get('api/common/env');
            const $search = this.http.get(`api/billings/receipt/${search.type}/${search.value}/${search.value2}`);


            forkJoin([$env, $search]).subscribe(v => {
                this.env = v[0];
                this.rows = v[1];
                this.captcha.reset();
                this.loading = false;
            }, er => {
                this.loading = false;
            });
        }
    }

    getStatus(row) {
        if (row.status === 0)
            return 'รอพิมพ์ต้นฉบับ';
        if (row.status === 1)
            return 'พิมพ์ต้นฉบับแล้ว';
        if (row.status === 2)
            return 'พิมพ์สำเนาแล้ว';
        else
            return row.status;
    }
}
