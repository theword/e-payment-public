import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { PreHeaderComponent } from './pre-header/pre-header.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppCommonModule } from '../components/common/app-common.module';
import { LinkInterestedComponent } from './link-interested.component';

@NgModule({
    declarations: [
        HeaderComponent,
        PreHeaderComponent,
        FooterComponent,
        LinkInterestedComponent
    ],
    imports: [RouterModule, AppCommonModule, CommonModule, FormsModule, ReactiveFormsModule, HttpClientModule],
    exports: [HeaderComponent, PreHeaderComponent, FooterComponent, LinkInterestedComponent],
    providers: [],
})
export class AppSharedModule { }
