package com.orange.epayment.endpoint.request;



import lombok.Data;
@Data
public class PostRequestImage {
    private String data;
    private String key;
    private String file;
}