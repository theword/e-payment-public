import { Directive } from '@angular/core';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Directive({
    selector: '[appLayout]',
})
export class AppLayoutDirective implements AfterViewInit {
    ngAfterViewInit(): void {

        Layout.init();
        Layout.initFixHeaderWithPreHeader();
    }

}