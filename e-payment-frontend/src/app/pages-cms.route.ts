import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/admin/home/home.component';
import { AppCommonModule } from './components/common/app-common.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NewsComponent } from './components/admin/news/news.component';
import { DownloadComponent } from './components/admin/download/download.component';
import { NewsAddComponent } from './components/admin/news/news-addedit.component';

import { LinkComponent } from './components/admin/link/link.component';
import { DownloadGenericComponent } from './components/admin/download/download-generic.component';


const routes: Routes = [
    {
        path: '', component: HomeComponent,
        children: [
            { path: 'home', redirectTo: 'news' },
            { path: 'news', component: NewsComponent },
            { path: 'news/addoredit/:id', component: NewsAddComponent },
            // { path: 'download-form', component: DownloadFormComponent },
            // { path: 'download-manual', component: DownloadManualComponent },
            // { path: 'download-agreement', component: DownloadAgreementComponent },
            { path: 'download/:type', component: DownloadGenericComponent },
            { path: 'link', component: LinkComponent },
            { path: '**', redirectTo: 'news' },

        ]
    }
];

@NgModule({
    declarations: [
        HomeComponent,
        NewsComponent,
        DownloadComponent,
        NewsAddComponent,
        // DownloadFormComponent,
        // DownloadManualComponent,
        // DownloadAgreementComponent,
        LinkComponent,
        DownloadGenericComponent
    ],
    imports: [AppCommonModule, CommonModule, HttpClientModule, ReactiveFormsModule, RouterModule.forChild(routes)],
    exports: [AppCommonModule, HttpClientModule, ReactiveFormsModule, RouterModule],
    providers: [
        // MemberRouteGuard
    ]
})
export class PageCMSRouteModule { }
