import {
    NgModule
} from '@angular/core';
import {
    CommonModule
} from '@angular/common';
import {
    DatePickerComponent
} from './datepicker.component';
import {
    FileInputComponent
} from './file-Input.component';
import {
    CaptchaComponent
} from './captcha.component';
import {
    BotDetectCaptchaModule
} from 'angular-captcha';


import {
    AppAccordionDirective
} from './app-accordian.directive';
import {
    ValidationService
} from './validation.service';
import {
    AppNumberOnlyDirective
} from './app-number-only.directive';
import {
    FormsModule,
    ReactiveFormsModule
} from '@angular/forms';
import {
    EmailInputComponent
} from './common-input.component';
import {
    InputAddressComponent
} from './app-input-address.component';
import {
    AuthenticationService
} from './authentication.service';
import {
    AppLayoutDirective
} from './layout.directive';
import {
    DialogComponent
} from './app-dialog.component';
import {
    DisableControlDirective
} from './disable-control.directive';
import {
    ThaiDatePipe
} from './thaidate.pipe';
import {
    WyswygeditorComponent
} from './wyswygeditor/wyswygeditor.component';
import {
    ShortContentPipe
} from './short-content.pipe';
import {
    DownloadTableComponent
} from './download/download-table.component';
import {
    LayoutComponent
} from './layout.component';
import {
    PaginationConfig,
    PaginationModule
} from './pagination';
import {
    PostViewPipe
} from './postview.pipe';
import { SafeHtmlPipe } from './safehtml.pipe';


@NgModule({
    declarations: [DatePickerComponent, FileInputComponent, CaptchaComponent, AppAccordionDirective, AppNumberOnlyDirective, EmailInputComponent, InputAddressComponent, AppLayoutDirective, DialogComponent, DisableControlDirective, ThaiDatePipe, WyswygeditorComponent, ShortContentPipe, DownloadTableComponent, LayoutComponent, PostViewPipe,SafeHtmlPipe],
    imports: [CommonModule,
        PaginationModule,
        FormsModule,
        ReactiveFormsModule,
        BotDetectCaptchaModule.forRoot({
            captchaEndpoint: 'botdetectcaptcha'
        })
    ],
    exports: [DatePickerComponent, FileInputComponent, CaptchaComponent, AppAccordionDirective, AppNumberOnlyDirective, EmailInputComponent, InputAddressComponent, AppLayoutDirective, DialogComponent, DisableControlDirective, ThaiDatePipe, WyswygeditorComponent, ShortContentPipe, DownloadTableComponent, LayoutComponent, PaginationModule, PostViewPipe,SafeHtmlPipe],
    providers: [
        ValidationService,
        AuthenticationService,
        PaginationConfig
    ],
})
export class AppCommonModule { }
