package com.orange.epayment.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class FileInfo extends AbstractEntity {

	@Column(length = 5000)
	private String fileName;

	private long length;

	@Enumerated(EnumType.ORDINAL)
	private FileSourceType sourceType;

	private String source;

	private Date createdDate;

	private Date modifiedDate;

	@OneToOne(fetch = FetchType.LAZY, optional = true)
	private File file;

}
