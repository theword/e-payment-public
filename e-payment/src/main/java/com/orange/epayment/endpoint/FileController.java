package com.orange.epayment.endpoint;

import com.orange.epayment.data.FileInfoRepository;
import com.orange.epayment.data.PostMetaRepository;
import com.orange.epayment.domain.FileInfo;
import com.orange.epayment.domain.PostMeta;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/files")
        @ Slf4j
public class FileController {

    private PostMetaRepository postMeta;
    private FileInfoRepository fileInfoRepo;

    @Autowired
    public FileController(PostMetaRepository postMeta, FileInfoRepository fileInfoRepo) {
        this.postMeta = postMeta;
        this.fileInfoRepo = fileInfoRepo;
    }

    @RequestMapping(value = "/temp/{name}.pdf", method = RequestMethod.GET)
    public void getFile(@PathVariable("name") String name, HttpServletResponse response) throws IOException {
        try {

            File temp = File.createTempFile("temp-file-name", ".tmp");

            String absolutePath = temp.getAbsolutePath();
            String tempFilePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));

            Files.delete(temp.toPath());

            Path path = Paths.get(tempFilePath, name + ".tmp");
            log.info(path.toString());
            File file = new File(path.toString());

            FileInputStream fileStream = new FileInputStream(file);

            StreamUtils.copy(fileStream, response.getOutputStream());
            fileStream.close();
            response.setContentType("application/pdf");

            response.flushBuffer();
        } catch (FileNotFoundException e) {
            log.error("{}", e);
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "File not found  -  " + name);
        } catch (Exception e) {
            log.error("{}", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal Server error");
        }
    }

    @Transactional
    @RequestMapping(value = "/posts/{id}.png", method = RequestMethod.GET)
    public void getPicture(@PathVariable("id") Long metaId, HttpServletResponse response) throws IOException {

        PostMeta meta = this.postMeta.findOne(metaId);

        Long fileinfoId = Long.valueOf(meta.getValue());

        FileInfo fileInfo = this.fileInfoRepo.findOne(fileinfoId);

        byte[] data = fileInfo.getFile().getData();
        response.setContentType("image/png");
        response.getOutputStream().write(data, 0, data.length);

        response.flushBuffer();
    }

    @Transactional
    @RequestMapping(value = "/posts/{postId}/{name}.{extension}", method = RequestMethod.GET)
    public void getFile(@PathVariable("name") String name, @PathVariable("postId") Long postId,
            @PathVariable("extension") String extension, HttpServletResponse response) throws IOException {

        PostMeta meta = this.postMeta.getByPostIdAndKey(postId, PostMeta.ATTACH_FILEINFO_ID);
        if (meta == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, name);
            return;
        }

        PostMeta meta2 = this.postMeta.getByPostIdAndKey(postId, PostMeta.PAGE_VIEW);
        if (meta2 != null) {
            meta2.setValue(String.valueOf(Integer.valueOf(meta2.getValue()) + 1));
            this.postMeta.save(meta2);
        }

        Long fileinfoId = Long.valueOf(meta.getValue());

        FileInfo fileInfo = this.fileInfoRepo.findOne(fileinfoId);

        byte[] data = fileInfo.getFile().getData();

        String contentType = Files.probeContentType(Paths.get(name + "." + extension));

        response.getOutputStream().write(data, 0, data.length);
        response.setContentType(contentType);

        response.flushBuffer();

    }
}
