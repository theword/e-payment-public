package com.orange.epayment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
@Getter
public class PublicEnvironment {

    @Value("${wisesoft.billing.baseUrl}")
    private String BillingBaseUrl;

    @Value("${app.baseUrl}")
    private String baseUrl;

    @Value("${wisesoft.login.baseUrl}")
    private String externalAdminUrl;

}