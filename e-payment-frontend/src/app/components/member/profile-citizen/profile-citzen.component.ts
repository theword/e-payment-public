import { Component, OnInit, ViewChild, ElementRef, Input, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CaptchaComponent } from '../../common/captcha.component';
import { InputAddressComponent } from '../../common/app-input-address.component';
import * as moment from 'moment';
import { AuthenticationService } from '../../common/authentication.service';
import { DialogComponent } from '../../common/app-dialog.component';

@Component({
    selector: 'app-profile-citizen',
    templateUrl: './profile-citizen.component.html',
})
export class ProfileCitizenComponent implements OnInit, AfterViewInit {

    @Input() citizenId: string;
    editForm: FormGroup;

    provinces: Observable<any[]>;
    districts: Observable<any[]>;
    subdistricts: any[];
    titles: any[];
    inProgress: boolean;
    message: any;
    titleMessage: any;

    oldBillingAddress; any;
    @ViewChild('modalEditResult') modalEditResult;
    @ViewChild(CaptchaComponent) recaptchaRef: CaptchaComponent;
    @ViewChild('EditResultDialog') editResultDialog: DialogComponent;

    // Addressses
    @ViewChild('billingAddress') billingAddress: InputAddressComponent;
    @ViewChild('registerAddress') registerAddress: InputAddressComponent;

    constructor(private http: HttpClient, private fb: FormBuilder, private el: ElementRef, private auth: AuthenticationService) { }

    createForm() {
        this.editForm = this.fb.group({
            birthDate: new FormControl({ value: null, disabled: true }),
            titleNameTH: new FormControl({ value: null, disabled: true }, [Validators.required]),
            fnameTH: new FormControl({ value: null, disabled: true }, [Validators.required]),
            lnameTH: new FormControl({ value: null, disabled: true }, [Validators.required]),
            mnameTH: new FormControl({ value: null, disabled: true }),
            titleNameEN: new FormControl({ value: null, disabled: true }),
            fnameEN: new FormControl({ value: null, disabled: true }),
            lnameEN: new FormControl({ value: null, disabled: true }),
            mnameEN: new FormControl({ value: null, disabled: true }),
            email: new FormControl(null, [Validators.required]),
            mobile: new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
            captcha: new FormControl(null, [Validators.required]),
            age: new FormControl({ value: null, disabled: true }),
            billingAddress: this.billingAddress.address,
            registerAddress: this.registerAddress.address,
        });


    }

    ngOnInit() {
        this.http.get('api/common/titles').subscribe((r: any) => {
            this.titles = r;
        });

        this.createForm();
        this.getCitizenData();
    }

    ngAfterViewInit(): void {

    }

    getCitizenData() {

        this.auth.get('api/member/me').subscribe((r: any) => {
            this.setForm(r);
        });

    }
    revert() {
        this.recaptchaRef.reset();
        this.getCitizenData();
    }

    onSubmit() {
        this.inProgress = true;

        const data = this.editForm.getRawValue();
        //remap
        data.titleNameTH = this.titles.find(x => x.desc === data.titleNameTH).id;
        data.titleNameEN = this.titles.find(x => x.desc2 === data.titleNameEN).id;


        this.auth.post('api/member/me', data).subscribe((r: any) => {
            this.showDialog('แจ้งผลการทำงานของระบบ', 'ระบบแก้ไขข้อมูลส่วนตัวของท่านเรียบร้อยแล้ว');
            this.inProgress = false;
        }, (err: any) => {
            this.showDialog('แจ้งข้อผิดพลาด', 'ไม่สามารถแก้ไขข้อมูลได้');
            this.inProgress = false;
        });


    }

    showDialog(titleMessage, message) {
        this.titleMessage = titleMessage;
        this.message = message;
        const comp = this;
        this.editResultDialog.open().subscribe(v => {
            comp.recaptchaRef.reset();
        });

    }

    copyAddress(event, source, destination): void {
        if (event.target.checked) {
            let v = source.value;

            if (destination.value.id == null)
                v.id = null;
            else
                v.id = destination.value.id;


            destination.setValue(v);
        } else {
            this.billingAddress.value = this.oldBillingAddress;
        }

    }
    setForm(response): void {
        this.editForm.get('birthDate').setValue(response.birthDate);
        this.editForm.get('titleNameTH').setValue(response.titleNameTH);
        this.editForm.get('fnameTH').setValue(response.fnameTH);
        this.editForm.get('lnameTH').setValue(response.lnameTH);
        this.editForm.get('mnameTH').setValue(response.mnameTH);
        this.editForm.get('titleNameEN').setValue(response.titleNameEN);
        this.editForm.get('fnameEN').setValue(response.fnameEN);
        this.editForm.get('lnameEN').setValue(response.lnameEN);
        this.editForm.get('mnameEN').setValue(response.mnameEN);
        this.editForm.get('email').setValue(response.email);
        this.editForm.get('mobile').setValue(response.mobile);
        this.editForm.get('age').setValue(moment().diff(+response.birthDate, 'years'));

        this.billingAddress.value = response.billingAddress;
        this.oldBillingAddress = response.billingAddress;
        this.registerAddress.value = response.registerAddress;
    }

    formInvalid() {
        return this.inProgress || this.editForm.invalid;
    }

}
