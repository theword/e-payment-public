package com.orange.epayment;

import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.logging.Level;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Cookie;

import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.logging.LoggingFeature;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

import com.orange.epayment.endpoint.WebResourceFactory;
import com.wisesoft.epayment.service.EpaymentApiService;
import com.wisesoft.epayment.service.WisesoftRootService;
import lombok.extern.java.Log;

@Log
@Configuration
public class AppBeanFactory {
    private AppEnvironment env;

    public AppBeanFactory(AppEnvironment env) {
        this.env = env;
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    @Lazy(value = true)
    public EpaymentApiService createEpaymentApiService(MediaType contentType) throws URISyntaxException, MalformedURLException {
       

        String proxyUrl  = this.env.getProxyUrl();

    
        Client client = null;
        if(!proxyUrl.isEmpty()){
            URL proximo = new URL(proxyUrl);
            String userInfo = proximo.getUserInfo();
            String user = userInfo.substring(0, userInfo.indexOf(':'));
            String password = userInfo.substring(userInfo.indexOf(':') + 1);
            System.setProperty("socksProxyHost", proximo.getHost());
            Authenticator.setDefault(new ProxyAuthenticator(user, password));

            client = ClientBuilder.newClient();

        }else{
            client = ClientBuilder.newClient();
        }

        client.register(new LoggingFeature(log));
		WebTarget webTarget = client.target(this.env.getWisesoftApiBaseUrl());

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.add("Content-Type", contentType.toString());
     
		return WebResourceFactory.newResource(EpaymentApiService.class, webTarget, false, headers,
				Collections.<Cookie>emptyList(), new Form());
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    @Lazy(value = true)
    public WisesoftRootService createWisesoftRootService(MediaType contentType) throws MalformedURLException {
       
        String proxyUrl  = this.env.getProxyUrl();

    
        Client client = null;
        if(!proxyUrl.isEmpty()){
            URL proximo = new URL(proxyUrl);
            String userInfo = proximo.getUserInfo();
            String user = userInfo.substring(0, userInfo.indexOf(':'));
            String password = userInfo.substring(userInfo.indexOf(':') + 1);
            System.setProperty("socksProxyHost", proximo.getHost());
            Authenticator.setDefault(new ProxyAuthenticator(user, password));

            client = ClientBuilder.newClient();

        }else{
            client = ClientBuilder.newClient();
        }

        client.register(new LoggingFeature(log));
        
		WebTarget webTarget = client.target(this.env.getWisesoftApiBaseUrl());

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.add("Content-Type", contentType.toString());
     
		return WebResourceFactory.newResource(WisesoftRootService.class, webTarget, false, headers,
				Collections.<Cookie>emptyList(), new Form());
    }


    private class ProxyAuthenticator extends Authenticator {
        private final PasswordAuthentication passwordAuthentication;
        private ProxyAuthenticator(String user, String password) {
          passwordAuthentication = new PasswordAuthentication(user, password.toCharArray());
        }
      
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
          return passwordAuthentication;
        }
      }
}