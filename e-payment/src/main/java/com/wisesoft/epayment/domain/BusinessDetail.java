package com.wisesoft.epayment.domain;

import lombok.Data;


@Data
public class BusinessDetail {
  String id;
  String businessCode;
  String companyName;
  String registerDate;
  String registeredCapital;
  String objectiveCode;
  String objectiveDesc;
  String building_name;
  String moo;
  String no;
  String road;
  String soi;
  String village;
  String postCode;
  String roomNo;
  String provinceName;
  String amphurName;
  String tambonName;
  String floor;
  String status;
}