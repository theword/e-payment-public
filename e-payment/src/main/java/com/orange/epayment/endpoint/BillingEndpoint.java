package com.orange.epayment.endpoint;

import com.orange.epayment.PublicEnvironment;
import com.orange.epayment.endpoint.request.BillingValidateRequest;
import com.orange.epayment.endpoint.response.BillingValidateResponse;
import com.wisesoft.epayment.domain.GetReceiptResult;
import com.wisesoft.epayment.service.EpaymentApiService;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import java.util.Base64;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.BeanFactory;

@Component
@Path("/billings")
public class BillingEndpoint {

    private BeanFactory beanFactory;
    private PublicEnvironment env;

    @Autowired
    public BillingEndpoint(BeanFactory beanFactory, PublicEnvironment env) {
        this.beanFactory = beanFactory;
        this.env = env;
    }

    private EpaymentApiService epaymentApiService(MediaType contentType) {
        return this.beanFactory.getBean(EpaymentApiService.class, contentType);
    }

    @GET
    @Path("/receipt/{type}/{value}/{value2}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchReceipt(@PathParam("type") String type, @PathParam("value") String value,
            @PathParam("value2") String value2) {

        String searchValue = value;
        String ref1 = value2;

        List<GetReceiptResult> result = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).getReceipt(type,
                searchValue, ref1);

        return Response.ok(result).build();
    }

    @POST
    @Path("/validate")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response validateReceipt(BillingValidateRequest request) throws IOException {
        File temp = File.createTempFile("tempfile", ".tmp");
        String[] byte64encoded = request.getFile().split(";");
        FileOutputStream outFile = new FileOutputStream(temp);
        byte[] data = Base64.getDecoder().decode(byte64encoded[1]);
        outFile.write(data);
        outFile.close();

        String fileURL = this.env.getBaseUrl() + "/files/temp/" + temp.getName().replace(".tmp", ".pdf");

        boolean result = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).checkReceipt(fileURL);

        BillingValidateResponse response = new BillingValidateResponse();
        response.setFileURL(fileURL);
        response.setResult(result);

        return Response.ok(response).build();
    }
}