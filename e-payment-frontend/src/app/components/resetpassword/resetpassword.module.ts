import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { ResetpasswordComponent } from './resetpassword.component';
import { AppCommonModule } from '../common/app-common.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    { path: '', component: ResetpasswordComponent }
];

@NgModule({
    imports: [AppCommonModule, CommonModule, HttpClientModule, ReactiveFormsModule, RouterModule.forChild(routes)],
    exports: [],
    declarations: [
        ResetpasswordComponent
    ],
    providers: [],
})
export class ResetPasswordModule { }
