import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { WyswygeditorComponent } from '../../common/wyswygeditor/wyswygeditor.component';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../common/authentication.service';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-news-add',
    templateUrl: 'news-addedit.component.html',
    styleUrls: ['../admin.component.css', 'news-addedit.component.css']
})

export class NewsAddComponent implements OnInit {

    @ViewChild('editor') editor: WyswygeditorComponent;

    @ViewChild('file') fileBrowserInput: ElementRef;

    form: FormGroup;

    postId: any;


    images: any[] = [];

    constructor(private route: Router, private activeRout: ActivatedRoute, private auth: AuthenticationService) { }

    ngOnInit() {

        this.form = new FormGroup({
            title: new FormControl(null, Validators.required),
            content: new FormControl(),
            type: new FormControl()
        });

        this.activeRout.paramMap.subscribe(v => {
            this.postId = v.get('id');
            this.form.get('type').setValue(v.get('type'));
        });



        this.form.valueChanges.subscribe(v => {
            console.log(v);
        });
    }


    editorChanged(value) {
        this.form.get('content').setValue(value);
    }

    editorReady($event) {
        if (this.postId != 0) {
            this.auth.get('api/posts/' + this.postId).subscribe(v => {
                this.form.get('title').setValue(v.title);
                this.form.get('type').setValue(this.getType(v));
                this.editor.value = v.content;

                for (var i = 0; i < v.meta.length; i++) {

                    if (v.meta[i].key == 'attach_fileinfo_id') {
                        this.readFileFromURL(v.meta[i], i).subscribe(r => {
                            this.images.push(r);
                        });
                    }
                }

            });
        } else {
            this.form.get('type').setValue('1');
        }
    }

    addPicture() {
        this.fileBrowserInput.nativeElement.click();
    }

    getType(post) {
        let news_type = (post.meta as any[]).find(i => i.key === 'news_type');
        return news_type ? news_type.value : 0;

    }

    fileBrowserChanged(event: any) {
        var files = event.target.files;



        for (var i = 0; i < files.length; i++) {
            let num = this.images.length;
            let key = i + num;
            var file = files[i];

            this.readFile(file, key.toString()).subscribe(r => {
                this.images.push(r);
            });

        }
    }

    readFileFromURL(meta, key): Observable<any> {
        return Observable.create(ob => {
            var request = new XMLHttpRequest();
            request.open('GET', `files/posts/${meta.id}.png`, true);

            request.responseType = 'blob';
            request.onload = function () {
                var reader = new FileReader();
                reader.readAsDataURL(request.response);
                reader.onload = function (e: any) {
                    var base64 = e.target.result;
                    ob.next({
                        key: key,
                        data: base64,
                    })
                };
            };
            request.send();
        });
    }



    readFile(file, key): Observable<any> {
        return Observable.create(ob => {
            var reader = new FileReader();
            reader.onload = function (e: any) {

                var base64 = e.target.result;

                ob.next({
                    key: key,
                    data: base64,
                })
            }
            reader.readAsDataURL(file);
        });
    }

    removeImage(img) {
        this.images = this.images.filter(i => i.key !== img.key);
    }

    save(draft: any) {
        var req = this.form.getRawValue();
        if (draft)
            req.status = 'Draft';
        else
            req.status = 'Published';

        req.images = this.images;
        req.postType = 'Post';

        this.auth.post(`api/cms/posts/${this.postId}`, req).subscribe(v => {
            if (!draft)
                this.route.navigateByUrl('/admin/advertise');
            else
                this.route.navigateByUrl('/public/news/' + v.id);
        });
    }

    cancel() {
        this.route.navigateByUrl('/cms/advertise');
    }

    get invalid() {
        if (!this.form)
            return true;
        return this.form.invalid;
    }
}