package com.orange.epayment.endpoint;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orange.epayment.endpoint.request.ForgotPasswordRequest;
import com.orange.epayment.endpoint.request.RegisterCitizenRequest;
import com.orange.epayment.endpoint.request.RegisterCorporateRequest;
import com.orange.epayment.endpoint.response.AddressResponse;
import com.wisesoft.epayment.domain.Address;
import com.wisesoft.epayment.domain.Citizen;
import com.wisesoft.epayment.domain.RegisterRequest;
import com.wisesoft.epayment.service.EpaymentApiService;

import lombok.extern.java.Log;

@Component
@Path("/register")
        @ Log
public class RegisterEndpoint {

    private BeanFactory beanFactory;

    @Autowired
    public RegisterEndpoint(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    private EpaymentApiService epaymentApiService() {
        return this.beanFactory.getBean(EpaymentApiService.class, MediaType.APPLICATION_JSON_TYPE);
    }

    private EpaymentApiService epaymentApiService(MediaType contentType) {
        return this.beanFactory.getBean(EpaymentApiService.class, contentType);
    }

    @POST
    @Path("/citizen")
    @Produces("application/json")
    @Consumes("application/json")
    public Response registerCitizen(RegisterCitizenRequest request) {

        RegisterRequest req = new RegisterRequest();

        req.setBirthDate(request.getBirthDate());
        req.setCitizenid(request.getCitizenid());
        req.setEmail(request.getEmail());
        req.setFnameEN(request.getFnameEN());
        req.setFnameTH(request.getFnameTH());
        req.setLnameEN(request.getLnameEN());
        req.setLnameTH(request.getLnameTH());
        req.setMnameEN(request.getMnameEN());
        req.setMnameTH(request.getMnameTH());
        req.setMobile(request.getMobile());
        req.setMode("1");
        req.setTitleNameEN(request.getTitleNameEN());
        req.setTitleNameTH(request.getTitleNameTH());
        req.setUserType("1");
        req.setVerifiedType(request.getVerifiedType());
        req.setPassword(request.getPassword());

        Address address = request.getRegisterAddress();
        req.setMoo(address.getMoo());
        req.setNo(address.getNo());
        req.setPostCode(address.getPostCode());
        req.setProvinceId(address.getProvinceId());
        req.setRoad(address.getRoad());
        req.setSoi(address.getSoi());
        req.setTambonId(address.getTambonId());
        req.setVillage(address.getVillage());
        req.setAmphurId(address.getAmphurId());

        String response = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).Register(req);
        if (!response.toLowerCase().contains("success")) {
            return Response.status(Status.BAD_REQUEST).entity(response).build();
        } else {

            Citizen citizenInfo = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE)
                    .GetCitizen(request.getCitizenid());
            this.insertAddress(citizenInfo, request.getBillingAddress(), "1");
            this.insertAddress(citizenInfo, request.getRegisterAddress(), "3");
            return Response.ok(response).build();
        }
    }

    @POST
    @Path("/corporate")
    @Produces("application/json")
    @Consumes("application/json")
    public Response registerCorporate(RegisterCorporateRequest request) {

        String pathFile = "";

        if (request.getFile() != null) {
            pathFile = writeFile(request.getFile());
        } else {
            pathFile = null;
        }
        com.wisesoft.epayment.domain.RegisterCorporateRequest req = new com.wisesoft.epayment.domain.RegisterCorporateRequest();

        req.setAmphurId(request.getAmphurId());
        req.setBirthDate(request.getBirthDate());
        req.setCitizenid(request.getCompanyId());
        req.setEmail(request.getEmail());

        req.setFnameEN(request.getCompanyNameEN());
        req.setFnameTH(request.getCompanyNameTH());
        // req.setLnameEN(request.getLnameEN());
        // req.setLnameTH(request.getLnameTH());
        // req.setMnameEN(request.getMnameEN());
        // req.setMnameTH(request.getMnameTH());
        req.setMobile(request.getMobile());
        req.setMode("1");

        req.setTitleNameEN(request.getTitleNameEN());
        req.setTitleNameTH(request.getTitleNameTH());
        req.setUserType("2");
        req.setVerifiedType(request.getVerifiedType());

        req.setPassword(request.getPassword());

        Address address = request.getWorkingAddress();
        req.setMoo(address.getMoo());
        req.setNo(address.getNo());
        req.setPostCode(address.getPostCode());
        req.setProvinceId(address.getProvinceId());
        req.setRoad(address.getRoad());
        req.setSoi(address.getSoi());
        req.setTambonId(address.getTambonId());
        req.setVillage(address.getVillage());
        req.setAmphurId(address.getAmphurId());

        req.setCompany_auth_email(request.getEmail());
        req.setCompany_auth_id(request.getCompany_auth_id());
        req.setCompany_auth_phone(request.getMobile());
        req.setCompany_objective(request.getCompanyObjective());
        req.setCompany_status(request.getCompanyStatus());
        req.setCompany_auth_birth_date(request.getBirthDate());

        req.setCompany_auth_titleNameEN(request.getCompany_auth_titleNameEN());
        req.setCompany_auth_titleNameTH(request.getCompany_auth_titleNameTH());
        req.setCompany_auth_fnameEN(request.getCompany_auth_fnameEN());
        req.setCompany_auth_fnameTH(request.getCompany_auth_fnameTH());
        req.setCompany_auth_lnameEN(request.getCompany_auth_lnameEN());
        req.setCompany_auth_lnameTH(request.getCompany_auth_lnameTH());
        req.setCompany_auth_mnameEN(request.getCompany_auth_mnameEN());
        req.setCompany_auth_mnamTH(request.getCompany_auth_mnamTH());

        req.setFile(pathFile);

        String response = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).RegisterCorporate(req);
        if (!response.toLowerCase().contains("success"))
            return Response.status(Status.BAD_REQUEST).entity(response).build();
        else {
            try {
                Citizen citizenInfo = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE)
                        .GetCitizen(request.getCompanyId());
                this.insertAddress(citizenInfo, request.getBillingAddress(), "1");
                this.insertAddress(citizenInfo, request.getWorkingAddress(), "2");
            } catch (Exception e) {
            }
            return Response.ok(response).build();
        }

    }

    private String writeFile(String fileEncoded) {
        Date date = new Date();
        LocalDate today = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String path = ".\\swfdata\\" + today.getYear() + "\\" + today.getMonthValue() + "\\" + today.getDayOfMonth();
        String[] byte64encoded = fileEncoded.split(";");
        String filename = byte64encoded[0];

        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        try (FileOutputStream outFile = new FileOutputStream(path + "\\" + filename)) {
            byte[] data = Base64.getDecoder().decode(byte64encoded[1]);
            outFile.write(data);
        } catch (FileNotFoundException e) {
        } catch (IOException ioe) {
        }

        return path + "\\" + filename;
    }

    @POST
    @Path("/forgotpassword")
    @Produces("application/json")
    @Consumes("application/json")
    public Response forgotPassword(ForgotPasswordRequest req) {

        Citizen cit = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).GetCitizen(req.getCitizenId());

        String email = cit.getUserType().equals("2") ? cit.getCompany_auth_email() : cit.getEmail();

        if (email.equals(req.getEmail())) {
            String result = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).forgotPassword(req.getCitizenId(),
                    req.getEmail());
            return Response.ok(result).build();
        } else {
            return Response.status(400).build();
        }
    }

    private void insertAddress(Citizen citizen, Address address, String type) {
        if (address == null)
            return;

        address.setUserId(citizen.getUserId());
        address.setAddressType(type);

        ModelMapper modelMapper = new ModelMapper();

        com.wisesoft.epayment.domain.Address r = modelMapper.map(address, com.wisesoft.epayment.domain.Address.class);
        this.epaymentApiService().insertAddress(r);
    }

}