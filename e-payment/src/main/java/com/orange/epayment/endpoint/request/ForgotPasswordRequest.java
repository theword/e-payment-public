package com.orange.epayment.endpoint.request;

import lombok.Data;

@Data
public class ForgotPasswordRequest {
    private String email;
    private String citizenId;
}