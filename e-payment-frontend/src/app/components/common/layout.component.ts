import { Component, OnInit, ViewChild } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs';
import { DialogComponent } from './app-dialog.component';

@Component({
    selector: 'app-layout',
    templateUrl: 'layout.component.html'
})

export class LayoutComponent implements OnInit {
    text;
    private confirmHandler: Subject<any>;
    private alertHandler: Subject<any>;
    @ViewChild('confirmDialog') confirmDialog: DialogComponent;
    @ViewChild('infoDialog') infoDialog: DialogComponent;

    constructor() { }

    ngOnInit() { }


    confirm(text): Observable<any> {
        this.text = text;

        this.confirmHandler = new Subject<any>();
        this.confirmDialog.open();

        return this.confirmHandler.asObservable();
    }

    alert(text): Observable<any> {
        this.text = text;

        this.alertHandler = new Subject<any>();
        this.infoDialog.open();

        return this.alertHandler.asObservable();
    }

    ok() {
        this.confirmDialog.close();
        this.infoDialog.close();

        if (this.confirmHandler)
            this.confirmHandler.next(null);
    }
    cancel() {
        this.confirmDialog.close();
    }
}