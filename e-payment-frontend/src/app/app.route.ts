import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Component } from '@angular/core/src/metadata/directives';

const routes: Routes = [
    { path: 'home', loadChildren: './home/home.module#HomeModule' },
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    { path: 'resetpassword', loadChildren: './components/resetpassword/resetpassword.module#ResetPasswordModule' },
    { path: 'page', loadChildren: './pages-cms.route#PageCMSRouteModule' },
    { path: 'member', loadChildren: './pages-member.route#PageMemberRouteModule' },
    { path: 'public', loadChildren: './pages-public.route#PagePublicRouteModule' },
    { path: 'admin', loadChildren: './pages-cms.route#PageCMSRouteModule' },
    { path: '**', redirectTo: '/home' }
];

@NgModule({
    declarations: [

    ],
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule { }
