import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter, ChangeDetectorRef, Directive, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-wyswygeditor',
  templateUrl: './wyswygeditor.component.html',
  styleUrls: ['./wyswygeditor.component.css']
})
export class WyswygeditorComponent implements OnInit, AfterViewInit {

  @ViewChild("editor") editor: ElementRef

  @Output() onReady: EventEmitter<any> = new EventEmitter();
  @Output() onChange: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    let comp = this;

    tinymce.init({
      target: this.editor.nativeElement,
      menubar: false,
      toolbar: "undo redo searchreplace | styleselect forecolor backcolor  | bold italic numlist bullist | hr table | alignleft aligncenter alignright | link image | preview",
      paste_as_text: true,
      external_plugins: {
        'image': '/e-payment/assets/tinymce/plugins/image/plugin.min.js',
        'preview': '/e-payment/assets/tinymce/plugins/preview/plugin.min.js',
        'link': '/e-payment/assets/tinymce/plugins/link/plugin.min.js',
        'lists': '/e-payment/assets/tinymce/plugins/lists/plugin.min.js',
        'hr': '/e-payment/assets/tinymce/plugins/hr/plugin.min.js',
        'paste': '/e-payment/assets/tinymce/plugins/paste/plugin.min.js',
        'searchreplace': '/e-payment/assets/tinymce/plugins/searchreplace/plugin.min.js',
        'table': '/e-payment/assets/tinymce/plugins/table/plugin.min.js',
        'textcolor': '/e-payment/assets/tinymce/plugins/textcolor/plugin.min.js'
      },
      theme_url: "/e-payment/assets/tinymce/themes/modern/theme.min.js",
      skin_url: "/e-payment/assets/tinymce/skins/lightgray",
      height: 300,
      init_instance_callback: function (editor) {

        comp.onReady.emit();

        editor.on('Change', function (e) {
          comp.onChange.emit(editor.getContent());
        });
      }
    });
  }
  get value() {
    return tinymce.activeEditor.getContent();
  }
  set value(val: any) {
    tinymce.activeEditor.setContent(val);
    this.onChange.emit(tinymce.activeEditor.getContent());
  }
}


