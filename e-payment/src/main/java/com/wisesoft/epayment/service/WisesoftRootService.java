package com.wisesoft.epayment.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

@Path("/")
public interface WisesoftRootService {

    @POST
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    @Path("/login")
    String login(MultivaluedMap<String, String> request);
}