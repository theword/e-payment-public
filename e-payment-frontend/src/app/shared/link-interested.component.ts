import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-link-interested',
    templateUrl: 'link-interested.component.html'
})

export class LinkInterestedComponent implements OnInit, AfterViewInit {


    @ViewChild('partnerMenu') partnerMenu: ElementRef;
    @ViewChild('linkMenu') linkMenu: ElementRef;

    links: Observable<any>;
    constructor(private http: HttpClient, private df: ChangeDetectorRef) { }

    ngOnInit() { }

    ngAfterViewInit(): void {



        jQuery(this.linkMenu.nativeElement).slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: false
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        this.links = this.http.get("api/posts/link");

        this.links.subscribe(r => {
            setTimeout(() => {
                jQuery(this.partnerMenu.nativeElement).slick({
                    dots: false,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 6,
                                slidesToScroll: 6,
                                infinite: true,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 800,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,
                                infinite: true,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: false
                            }
                        }
                        // You can unslick at a given breakpoint now by adding:
                        // settings: "unslick"
                        // instead of a settings object
                    ]
                });
                this.df.detectChanges();
                jQuery(window).trigger('resize');
            }, 0);

        });

        this.df.detectChanges();
    }

    getThumbnail(post) {
        let img = (post.meta as any[]).find(i => i.key == "attach_fileinfo_id");
        return img ? `files/posts/${img.id}.png` : undefined;
    }
}
