package com.orange.epayment.endpoint;

import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.val;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orange.epayment.PublicEnvironment;
import com.wisesoft.epayment.domain.District;
import com.wisesoft.epayment.domain.GetDistrictRequest;
import com.wisesoft.epayment.domain.GetSubDistrictRequest;
import com.wisesoft.epayment.domain.Province;
import com.wisesoft.epayment.domain.SubDistrict;
import com.wisesoft.epayment.domain.Title;
import com.wisesoft.epayment.service.EpaymentApiService;
import org.springframework.beans.factory.BeanFactory;

@Component
@Path("/common")
public class CommonEndpoint {

    private BeanFactory beanFactory;
    private PublicEnvironment env;

    @Autowired
    public CommonEndpoint(BeanFactory beanFactory, PublicEnvironment env) {
        this.beanFactory = beanFactory;
        this.env = env;
    }

    private EpaymentApiService epaymentApiService(MediaType contentType) {
        return this.beanFactory.getBean(EpaymentApiService.class, contentType);
    }

    @GET
    @Path("/titles")
    @Produces("application/json")
    public Response getTitles() {
        List<Title> titles = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).getTitle("{}");
        return Response.ok(titles).build();
    }

    @GET
    @Path("/provinces")
    @Produces("application/json")
    public Response getProvinces() {
        List<Province> provinces = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).getProvince();

        return Response.ok(provinces).build();
    }

    @GET
    @Path("/provinces/{provinceId}/districts")
    @Produces("application/json")
    public Response getDistrictsByProvinceId(@PathParam("provinceId") int provinceId) {

        @val
        List<District> districts;
        try {
            val request = new GetDistrictRequest();
            request.setProvinceId(provinceId);
            Form form = new Form("provinceId", Integer.toString(provinceId));
            districts = this.epaymentApiService(MediaType.APPLICATION_FORM_URLENCODED_TYPE).getDistrict(form);
        } catch (Exception e) {
            throw new InternalServerErrorException(e);
        }

        return Response.ok(districts).build();
    }

    @GET
    @Path("/districts/{districtId}/subdistricts")
    @Produces("application/json")
    public Response getSubDistrictsByDistrictId(@PathParam("districtId") int districtId) {

        @val
        List<SubDistrict> subdistricts;
        try {
            val request = new GetSubDistrictRequest();
            request.setProvinceId(districtId);
            Form form = new Form("districtId", Integer.toString(districtId));
            subdistricts = this.epaymentApiService(MediaType.APPLICATION_FORM_URLENCODED_TYPE).getSubDistrict(form);
        } catch (Exception e) {
            throw new InternalServerErrorException(e);
        }

        return Response.ok(subdistricts).build();
    }

    @GET
    @Path("/env")
    @Produces("application/json")
    public Response getEnvironment() {
        return Response.ok(this.env).build();
    }
}