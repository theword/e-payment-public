package com.orange.epayment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
@Getter
public class AppEnvironment {

    @Value("${wisesoft.api.baseUrl}")
    private String wisesoftApiBaseUrl;

    @Value("${PROXIMO_URL:}")
    private String proxyUrl;

}