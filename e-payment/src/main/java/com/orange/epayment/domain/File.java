package com.orange.epayment.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class File extends AbstractEntity {

	@Lob
	@Column(length = 20971520)
	private byte[] data;
}
