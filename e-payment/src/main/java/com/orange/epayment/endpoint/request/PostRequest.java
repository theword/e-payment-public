package com.orange.epayment.endpoint.request;

import java.util.List;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.orange.epayment.domain.PostStatus;
import com.orange.epayment.domain.PostType;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class PostRequest {
	private String title;
	private Long postId;
	private String content;
	private PostStatus status;
	private PostType postType;
	private int type;

	@Valid
	private List<PostRequestImage> images;
	
	
}

