package com.orange.epayment.domain;

import java.util.Collection;
import java.util.Date;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;

public interface PostInfo {

    @Value("#{target.id}")
    Long getId();

    String getTitle();

    Date getCreatedDate();

    PostStatus getStatus();

    PostType getType();

    String getContent();

    Collection<PostMetaInfo> getMeta();

    interface PostMetaInfo {

        Long getId();

        String getKey();

        String getValue();
    }
}