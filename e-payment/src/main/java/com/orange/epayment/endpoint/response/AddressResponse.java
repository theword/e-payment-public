package com.orange.epayment.endpoint.response;

import com.wisesoft.epayment.domain.Address;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AddressResponse extends Address {

}