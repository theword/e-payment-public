package com.wisesoft.epayment.domain;

import lombok.Data;

@Data
public class RegisterRequest {
     private String citizenid;
     private String birthDate;
     private String titleNameTH ;
     private String fnameTH;
     private String lnameTH; 
     private String mnameTH;
     private String titleNameEN;
     private String fnameEN; 
     private String lnameEN; 
     private String mnameEN; 
     private String email; 
     private String mobile; 
     private String no; 
     private String village; 
     private String moo;
     private String soi; 
     private String road; 
     private String provinceId; 
     private String amphurId;
     private String tambonId;
     private String postCode; 
     private String userType;
     private String mode;
     private String verifiedType;
     private String password;
     private String company_status;
     private String company_objective;
     private String company_auth_id;
     private String company_auth_email;
     private String company_auth_phone;
     private String company_auth_birth_date;
     private String company_auth_fnameTH;
     private String company_auth_lnameTH;
     private String company_auth_mnamTH;
     private String company_auth_titleNameEN;
     private String company_auth_fnameEN;
     private String company_auth_lnameEN;
     private String company_auth_mnameEN;
     private String company_auth_titleNameTH;
     private String file;
     private String userId;

}