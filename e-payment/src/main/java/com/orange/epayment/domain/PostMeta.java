package com.orange.epayment.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class PostMeta extends AbstractEntity {

	public static String ATTACH_FILEINFO_ID = "attach_fileinfo_id";
	public static String PAGE_VIEW = "page_view";
	public static String STATS = "stats";
	public static String NEWS_TYPE = "news_type";
	public static String ATTACH_TYPE = "ATTACH_TYPE";
	public static String ATTACH_LENGTH = "ATTACH_LENGTH";
	public static String ATTACH_NAME = "ATTACH_NAME";
	@ManyToOne(optional = false,fetch = FetchType.EAGER)
	@JsonIgnore 
	private Post post;

	private String key;

	private String value;
}
