package com.orange.epayment.endpoint.request;

import lombok.Data;

@Data
public class FileRequest {
    private String name;
    private String file;
}