package com.wisesoft.epayment.domain;

import lombok.Data;

@Data
public class SubDistrict {
    private int id;
    private String nameTH;
    private String nameEN;
    private Integer rootId;
    private Integer postCode;
}