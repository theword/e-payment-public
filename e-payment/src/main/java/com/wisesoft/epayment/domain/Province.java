package com.wisesoft.epayment.domain;

import lombok.Data;

@Data
public class Province {
    private int id;
    private String nameTH;
    private String nameEN;
    private Integer rootId;
}