package com.orange.epayment;

import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.captcha.botdetect.web.servlet.SimpleCaptchaServlet;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class })
public class EpaymentApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(EpaymentApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(EpaymentApplication.class);
	}

	@Bean
	public ServletRegistrationBean jerseyServlet() {
		ServletRegistrationBean registration = new ServletRegistrationBean(new ServletContainer(), "/api/*");
		registration.addInitParameter("javax.ws.rs.Application", "com.orange.epayment.endpoint.JerseyConfig");
		return registration;
	}

	@Bean
	public ServletRegistrationBean captchaServlet() {
		ServletRegistrationBean registration = new ServletRegistrationBean(new SimpleCaptchaServlet(),
				"/botdetectcaptcha/*");
		registration.setLoadOnStartup(1);
		return registration;
	}
}
