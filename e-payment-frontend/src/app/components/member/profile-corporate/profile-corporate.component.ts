import { Component, OnInit, ViewChild, ElementRef, Input, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CaptchaComponent } from '../../common/captcha.component';
import { ValidationService } from '../../common/validation.service';
import { InputAddressComponent } from '../../common/app-input-address.component';
import * as moment from 'moment';
import { AuthenticationService } from '../../common/authentication.service';
import { DialogComponent } from '../../common/app-dialog.component';
@Component({
    selector: 'app-profile-corporate',
    templateUrl: './profile-corporate.component.html',
})
export class ProfileCorporateComponent implements OnInit, AfterViewInit {

    @Input() corporateId: string;
    editForm: FormGroup;

    provinces: Observable<any[]>;
    districts: Observable<any[]>;
    subdistricts: any[];
    titles: any[];
    inProgress: boolean;
    titleMessage: any;
    message: any;
    companyId: any;
    companyNameEN: any;
    companyNameTH: any;
    companyStatus: any;
    oldBillingAddress; any;
    // Addressses
    @ViewChild('billingAddress') billingAddress: InputAddressComponent;
    @ViewChild('workingAddress') workingAddress: InputAddressComponent;

    @ViewChild('EditResultDialog') editResultDialog: DialogComponent;

    @ViewChild(CaptchaComponent) recaptchaRef: CaptchaComponent;
    constructor(private http: HttpClient, private fb: FormBuilder, private el: ElementRef, private vs: ValidationService, private auth: AuthenticationService) { }

    createForm() {
        this.editForm = this.fb.group({
            company_auth_titleNameTH: new FormControl(null, [Validators.required]),
            company_auth_fnameTH: new FormControl(null, [Validators.required]),
            company_auth_lnameTH: new FormControl(null, [Validators.required]),
            company_auth_mnamTH: new FormControl(null),
            company_auth_titleNameEN: new FormControl(null),
            company_auth_fnameEN: new FormControl(null),
            company_auth_lnameEN: new FormControl(null),
            company_auth_mnameEN: new FormControl(),
            company_auth_id: new FormControl(null, [Validators.required, this.vs.validatePersonNoFormat]),
            company_auth_birth_date: new FormControl(null, [Validators.required]),

            citizenId: new FormControl(null, [Validators.required]),
            fnameTH: new FormControl(null),
            lnameTH: new FormControl(null),
            mnameTH: new FormControl(null),
            fnameEN: new FormControl(null),
            lnameEN: new FormControl(null),
            mnameEN: new FormControl(null),
            company_auth_email: new FormControl(null, [Validators.required]),
            company_auth_phone: new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
            captcha: new FormControl(null, [Validators.required]),
            age: new FormControl({ value: null, disabled: true }),
            billingAddress: this.billingAddress.address,
            workingAddress: this.workingAddress.address
        });
    }

    ngOnInit() {
        this.http.get('api/common/titles').subscribe((r: any) => {
            this.titles = r;
        });
        this.createForm();
        this.getCitizenData();

    }

    ngAfterViewInit(): void {
    }

    getCitizenData() {

        this.auth.get(`api/member/me/`).subscribe((r: any) => {
            this.setForm(r);
        });

    }
    revert() {
        this.recaptchaRef.reset();
        this.getCitizenData();
    }

    onSubmit() {
        this.inProgress = true;
        const data = this.editForm.getRawValue();
        try {

            // // remap
            // data.company_auth_titleNameTH = this.titles.find(x => x.desc === data.company_auth_titleNameTH).id;
            // data.company_auth_titleNameEN = this.titles.find(x => x.desc2 === data.company_auth_titleNameEN).id;
        } catch (e) { }
        this.auth.post(`api/member/me/`, data).subscribe((r: any) => {
            this.showDialog('แจ้งผลการทำงานของระบบ', 'ระบบแก้ไขข้อมูลส่วนตัวของเท่านเรียบร้อยแล้ว');
            this.inProgress = false;
        }, (err: any) => {
            this.showDialog('แจ้งข้อผิดพลาด', 'ไม่สามารถแก้ไขข้อมูลได้');
            this.inProgress = false;
        });


    }

    showDialog(titleMessage, message) {
        this.titleMessage = titleMessage;
        this.message = message;
        const comp = this;
        this.editResultDialog.open().subscribe(v => {
            comp.recaptchaRef.reset();
        });

    }

    copyAddress(event, source, destination): void {

        if (event.target.checked) {
            let v = source.getRawValue();

            if (destination.value.id == null)
                v.id = null;
            else
                v.id = destination.value.id;

            destination.setValue(v);
        } else {
            this.billingAddress.value = this.oldBillingAddress;
        }
    }

    setForm(response): void {
        this.editForm.get('citizenId').setValue(response.citizenid);
        this.editForm.get('fnameTH').setValue(response.fnameTH);
        this.editForm.get('fnameEN').setValue(response.fnameEN);


        this.editForm.get('age').setValue(moment().diff(+response.company_auth_birth_date, 'years'));
        this.editForm.get('company_auth_email').setValue(response.company_auth_email);
        this.editForm.get('company_auth_phone').setValue(response.company_auth_phone);
        this.editForm.get('company_auth_id').setValue(response.company_auth_id);
        this.editForm.get('company_auth_birth_date').setValue(response.company_auth_birth_date);
        this.editForm.get('company_auth_fnameTH').setValue(response.company_auth_fnameTH);
        this.editForm.get('company_auth_fnameEN').setValue(response.company_auth_fnameEN);
        this.editForm.get('company_auth_lnameTH').setValue(response.company_auth_lnameTH);
        this.editForm.get('company_auth_lnameEN').setValue(response.company_auth_lnameEN);
        this.editForm.get('company_auth_titleNameTH').setValue(response.company_auth_titleNameTH);
        this.editForm.get('company_auth_titleNameEN').setValue(response.company_auth_titleNameEN);
        this.editForm.get('company_auth_mnamTH').setValue(response.company_auth_mnamTH);
        this.editForm.get('company_auth_mnameEN').setValue(response.company_auth_mnameEN);




        this.billingAddress.value = response.billingAddress;
        this.oldBillingAddress = response.billingAddress;
        this.workingAddress.value = response.workingAddress;



        if (response.company_status === '1') {
            this.companyStatus = 'ดำเนินกิจการ';
        } else if (response.company_status.status === '2') {
            this.companyStatus = 'ยกเลิกกิจการ';
        } else {
            this.companyStatus = 'อื่นๆ';
        }

    }
    formInvalid() {
        return this.inProgress || this.editForm.invalid;
    }

}
