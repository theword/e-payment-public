import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  isAdmin: boolean;

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }


      if (evt instanceof NavigationEnd) {
        let url = (evt as NavigationEnd).url;

        this.isAdmin = url.startsWith('/admin');
      }
      window.scrollTo(0, 0);
    });

  }
}
