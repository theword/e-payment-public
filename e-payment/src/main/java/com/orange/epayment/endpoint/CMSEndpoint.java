package com.orange.epayment.endpoint;

import com.orange.epayment.data.FileInfoRepository;
import com.orange.epayment.data.FileRepository;
import com.orange.epayment.data.PostMetaRepository;
import com.orange.epayment.data.PostRepository;
import com.orange.epayment.domain.File;
import com.orange.epayment.domain.FileInfo;
import com.orange.epayment.domain.Post;
import com.orange.epayment.domain.PostInfo;
import com.orange.epayment.domain.PostMeta;
import com.orange.epayment.domain.PostStatus;
import com.orange.epayment.domain.PostType;
import com.orange.epayment.endpoint.request.FileRequest;
import com.orange.epayment.endpoint.request.PostRequest;
import com.orange.epayment.endpoint.request.PostRequestImage;
import com.orange.epayment.domain.FileSourceType;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*")
@Component
@Path("/cms")
        @ Slf4j

public class CMSEndpoint {

    private FileRepository fileRepo;
    private FileInfoRepository fileInfoRepo;
    private PostRepository postRepo;
    private PostMetaRepository postMetaRepo;

    @Autowired
    public CMSEndpoint(PostRepository postRepo, FileRepository fileRepo, FileInfoRepository fileInfoRepo,
            PostMetaRepository postMetaRepo) {
        this.fileRepo = fileRepo;
        this.fileInfoRepo = fileInfoRepo;
        this.postRepo = postRepo;
        this.postMetaRepo = postMetaRepo;

    }

    @POST
    @Path("/posts/{postId}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Transactional
    public Response updateOrAddPost(@PathParam("postId") Long postId, PostRequest req) {
        if (postId == 0) { // new
            Post post = new Post();
            post.setContent(req.getContent());
            post.setTitle(req.getTitle());
            post.setStatus(req.getStatus());
            post.setCreatedDate(new Date());
            post.setType(req.getPostType());

            this.postRepo.save(post);

            PostMeta meta = new PostMeta();
            meta.setPost(post);
            meta.setKey(PostMeta.PAGE_VIEW);
            meta.setValue("0");
            this.postMetaRepo.save(meta);

            PostMeta meta2 = new PostMeta();
            meta2.setPost(post);
            meta2.setKey(PostMeta.NEWS_TYPE);
            meta2.setValue(String.valueOf(req.getType()));
            ;
            this.postMetaRepo.save(meta2);

            for (PostRequestImage img : req.getImages()) {

                File file = new File();
                String base64Image = img.getData().split(",")[1];
                byte[] decodedByte = Base64.getDecoder().decode(base64Image);
                file.setData(decodedByte);
                this.fileRepo.save(file);

                FileInfo fileInfo = new FileInfo();
                fileInfo.setFile(file);
                fileInfo.setFileName(img.getKey() + ".png");
                fileInfo.setLength(file.getData().length);
                fileInfo.setSourceType(FileSourceType.Database);

                this.fileInfoRepo.save(fileInfo);

                PostMeta imgMeta = new PostMeta();
                imgMeta.setPost(post);
                imgMeta.setKey(PostMeta.ATTACH_FILEINFO_ID);
                imgMeta.setValue(String.valueOf(fileInfo.getId()));
                this.postMetaRepo.save(imgMeta);
            }

            return Response.ok(post).build();

        } else {
            Post post = this.postRepo.findOne(postId);
            post.setContent(req.getContent());
            post.setTitle(req.getTitle());
            post.setStatus(req.getStatus());

            for (PostMeta m : post.getMeta()) {
                if (m.getKey().equals(PostMeta.ATTACH_FILEINFO_ID))
                    this.postMetaRepo.delete(m.getId());
            }

            for (PostRequestImage img : req.getImages()) {

                File file = new File();
                String base64Image = img.getData().split(",")[1];
                byte[] decodedByte = Base64.getDecoder().decode(base64Image);
                file.setData(decodedByte);
                this.fileRepo.save(file);

                FileInfo fileInfo = new FileInfo();
                fileInfo.setFile(file);
                fileInfo.setFileName(img.getKey() + ".png");
                fileInfo.setLength(file.getData().length);
                fileInfo.setSourceType(FileSourceType.Database);

                this.fileInfoRepo.save(fileInfo);

                PostMeta imgMeta = new PostMeta();
                imgMeta.setPost(post);
                imgMeta.setKey(PostMeta.ATTACH_FILEINFO_ID);
                imgMeta.setValue(String.valueOf(fileInfo.getId()));
                this.postMetaRepo.save(imgMeta);
            }

            this.postRepo.save(post);
            return Response.ok(post).build();
        }
    }

    @GET
    @Path("/posts/all")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Transactional
    public Response getAllPosts() {
        Collection<PostInfo> postInfo = this.postRepo.findByTypeOrderByCreatedDateDesc(PostType.Post);
        return Response.ok(postInfo).build();
    }

    @DELETE
    @Path("/posts/{id}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Transactional
    public Response deletePostById(@PathParam("id") Long postId) {
        Post post = this.postRepo.findOne(postId);
        this.postMetaRepo.delete(post.getMeta());
        this.postRepo.delete(post);
        return Response.ok(postId).build();
    }

    @POST
    @Path("/downloads/{type}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Transactional
    public Response addDownloadFormType(FileRequest req, @PathParam("type") String type) {

        Post post = new Post();
        post.setType(PostType.Attachment);
        post.setTitle(req.getName());
        post.setCreatedDate(new Date());
        post.setStatus(PostStatus.Published);

        this.postRepo.save(post);

        PostMeta meta = new PostMeta();
        meta.setPost(post);
        meta.setKey(PostMeta.PAGE_VIEW);
        meta.setValue("0");
        this.postMetaRepo.save(meta);

        PostMeta meta2 = new PostMeta();
        meta2.setPost(post);
        meta2.setKey(PostMeta.ATTACH_TYPE);
        meta2.setValue(type);
        this.postMetaRepo.save(meta2);

        String[] byte64encoded = req.getFile().split(";");
        String filename = byte64encoded[0];
        byte[] data = Base64.getDecoder().decode(byte64encoded[1]);

        File file = new File();
        file.setData(data);
        this.fileRepo.save(file);

        FileInfo fileInfo = new FileInfo();
        fileInfo.setFile(file);
        fileInfo.setFileName(filename);
        fileInfo.setLength(file.getData().length);
        fileInfo.setSourceType(FileSourceType.Database);

        this.fileInfoRepo.save(fileInfo);

        PostMeta meta3 = new PostMeta();
        meta3.setPost(post);
        meta3.setKey(PostMeta.ATTACH_LENGTH);
        meta3.setValue(String.valueOf(file.getData().length));
        this.postMetaRepo.save(meta3);

        PostMeta meta4 = new PostMeta();
        meta4.setPost(post);
        meta4.setKey(PostMeta.ATTACH_NAME);
        meta4.setValue(String.valueOf(filename));
        this.postMetaRepo.save(meta4);

        PostMeta imgMeta = new PostMeta();
        imgMeta.setPost(post);
        imgMeta.setKey(PostMeta.ATTACH_FILEINFO_ID);
        imgMeta.setValue(String.valueOf(fileInfo.getId()));
        this.postMetaRepo.save(imgMeta);

        return Response.ok().build();
    }

    @DELETE
    @Path("/downloads/{type}/{postId}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Transactional
    public Response deleteFile(@PathParam("postId") Long postId, @PathParam("type") String type) {

        Post post = this.postRepo.findOne(postId);

        PostMeta meta = this.postMetaRepo.getByPostIdAndKey(postId, PostMeta.ATTACH_FILEINFO_ID);

        Long fileinfoId = Long.valueOf(meta.getValue());

        FileInfo fileInfo = this.fileInfoRepo.findOne(fileinfoId);

        this.fileInfoRepo.delete(fileInfo);
        this.fileRepo.delete(fileInfo.getFile());

        this.postMetaRepo.delete(post.getMeta());

        return Response.ok().build();
    }

}
