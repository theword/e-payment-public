package com.orange.epayment.endpoint.request;

import java.io.Serializable;

import lombok.Data;

@Data
public class Credentials implements Serializable {
	private String username;
	private String password;
	private String clientSecret;
	private String token;
}
