import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'postview'
})

export class PostViewPipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {
        if (!value || !value.meta)
            return 0;
        let meta = (value.meta as any[]).find(i => i.key == "page_view");
        return meta ? meta.value : 0;
    }
}
