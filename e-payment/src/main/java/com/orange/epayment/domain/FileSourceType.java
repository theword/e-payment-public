package com.orange.epayment.domain;

public enum FileSourceType {

    Database, FileSystem

}
