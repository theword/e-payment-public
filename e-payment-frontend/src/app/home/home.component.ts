import { Component, OnInit } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import * as moment from 'moment';


@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit, AfterViewInit {


    constructor() { }

    ngOnInit() { }

    ngAfterViewInit() {

    }
}
