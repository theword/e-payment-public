package com.orange.epayment.endpoint;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wisesoft.epayment.service.EpaymentApiService;

import java.security.Principal;
import java.util.List;

import com.orange.epayment.endpoint.request.Address;
import com.orange.epayment.endpoint.request.ChangePasswordRequest;
import com.orange.epayment.endpoint.request.EditCitizenRequest;
import com.orange.epayment.endpoint.request.EditCorporateRequest;
import com.orange.epayment.endpoint.request.RegisterCitizenRequest;

import com.orange.epayment.endpoint.response.AddressResponse;
import com.orange.epayment.endpoint.response.CitizenResponse;
import com.orange.epayment.endpoint.response.CorporateResponse;
import com.wisesoft.epayment.domain.BusinessDetail;
import com.wisesoft.epayment.domain.Citizen;
import com.wisesoft.epayment.domain.RegisterRequest;
import com.wisesoft.epayment.domain.RegisterCorporateRequest;

import lombok.extern.java.Log;

@Component
@Path("/member")
        @ Log
public class MemberEndpoint {

    private BeanFactory beanFactory;

    @Autowired
    public MemberEndpoint(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    private EpaymentApiService epaymentApiService(MediaType contentType) {
        return this.beanFactory.getBean(EpaymentApiService.class, contentType);
    }

    private EpaymentApiService epaymentApiService() {
        return this.beanFactory.getBean(EpaymentApiService.class, MediaType.APPLICATION_JSON_TYPE);
    }

    @GET
    @Path("/validate")
    @Produces("application/json")
    @Consumes("application/json")
    @Secured
    public Response ValidateToken(@Context SecurityContext securityContext) {
        Principal principal = securityContext.getUserPrincipal();
        return Response.ok(principal).build();
    }

    @GET
    @Path("/me")
    @Produces("application/json")
    @Consumes("application/json")
    @Secured
    public Response GetCitizen(@Context SecurityContext securityContext) {
        Principal principal = securityContext.getUserPrincipal();
        String citizenid = principal.getName();

        Citizen result = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).GetCitizen(citizenid);

        List<com.wisesoft.epayment.domain.Address> addresses = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE)
                .searchAddress(result.getUserId());

        ModelMapper modelMapper = new ModelMapper();
        CitizenResponse response = modelMapper.map(result, CitizenResponse.class);

        for (com.wisesoft.epayment.domain.Address a : addresses) {
            AddressResponse addressRes = modelMapper.map(a, AddressResponse.class);

            switch (a.getAddressType()) {
            case "1":
                response.setBillingAddress(addressRes);
                break;
            case "2":
                response.setWorkingAddress(addressRes);
                break;
            case "3":
                response.setRegisterAddress(addressRes);
                break;
            case "4":
                response.setCurrentAddress(addressRes);
                break;
            }
        }

        return Response.ok(response).build();
    }

    @POST
    @Path("/me/changepassword")
    @Produces("application/json")
    @Consumes("application/json")
    @Secured
    public Response ChangePassword(@Context SecurityContext securityContext, ChangePasswordRequest pass) {
        Principal principal = securityContext.getUserPrincipal();
        String citizenid = principal.getName();

        Citizen request = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).GetCitizen(citizenid);

        RegisterRequest req = new RegisterRequest();
        req.setUserId(request.getUserId());
        req.setCitizenid(request.getCitizenid());

        req.setPassword(pass.getPassword());

        req.setBirthDate(request.getBirthDate());
        req.setEmail(request.getEmail());
        req.setFnameEN(request.getFnameEN());
        req.setFnameTH(request.getFnameTH());
        req.setLnameEN(request.getLnameEN());
        req.setLnameTH(request.getLnameTH());
        req.setMnameEN(request.getMnameEN());
        req.setMnameTH(request.getMnameTH());
        req.setMobile(request.getMobile());
        req.setMode("2");

        req.setTitleNameEN(request.getTitleNameEN());
        req.setTitleNameTH(request.getTitleNameTH());

        req.setVerifiedType("1");

        req.setUserType(request.getUserType());

        if (request.getUserType().equals("2")) {
            req.setCompany_auth_id(request.getCompany_auth_id());
            req.setCompany_auth_titleNameTH(request.getCompany_auth_titleNameTH());
            req.setCompany_auth_fnameTH(request.getCompany_auth_fnameTH());
            req.setCompany_auth_lnameTH(request.getCompany_auth_lnameTH());
            req.setCompany_auth_titleNameEN(request.getCompany_auth_titleNameEN());
            req.setCompany_auth_fnameEN(request.getCompany_auth_fnameEN());
            req.setCompany_auth_lnameEN(request.getCompany_auth_lnameEN());
            req.setCompany_auth_mnameEN(request.getCompany_auth_mnameEN());
            req.setCompany_auth_mnamTH(request.getCompany_auth_mnamTH());
            req.setCompany_auth_email(request.getCompany_auth_email());
            req.setCompany_auth_birth_date(request.getCompany_auth_birth_date());
            req.setCompany_auth_phone(request.getCompany_auth_phone());
            req.setCompany_objective(request.getCompany_objective());
            req.setCompany_status(request.getCompany_status());

            // address
            req.setMoo(request.getMoo());
            req.setNo(request.getNo());
            req.setPostCode(request.getPostCode());
            req.setProvinceId(request.getProvinceId());
            req.setRoad(request.getRoad());
            req.setSoi(request.getSoi());
            req.setTambonId(request.getTambonId());
            req.setAmphurId(request.getAmphurId());
            req.setVillage(request.getVillage());
        } else {

            List<com.wisesoft.epayment.domain.Address> addresses = this
                    .epaymentApiService(MediaType.APPLICATION_JSON_TYPE).searchAddress(request.getUserId());

            for (com.wisesoft.epayment.domain.Address a : addresses) {
                switch (a.getAddressType()) {
                case "1":
                    req.setMoo(a.getMoo());
                    req.setNo(a.getNo());
                    req.setPostCode(a.getPostCode());
                    req.setProvinceId(a.getProvinceId());
                    req.setRoad(a.getRoad());
                    req.setSoi(a.getSoi());
                    req.setTambonId(a.getTambonId());
                    req.setAmphurId(a.getAmphurId());
                    req.setVillage(a.getVillage());
                    break;
                }
            }
        }

        String response = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).Register(req);
        if (!response.toLowerCase().contains("success"))
            return Response.status(Status.BAD_REQUEST).entity(response).build();
        else
            return Response.ok(response).build();
    }

    @POST
    @Path("/me")
    @Produces("application/json")
    @Consumes("application/json")
    @Secured
    public Response UpdateCitizen(@Context SecurityContext securityContext, CitizenResponse request) {

        Principal principal = securityContext.getUserPrincipal();
        String citizenid = principal.getName();

        Citizen citizenInfo = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).GetCitizen(citizenid);

        String citizenType = citizenInfo.getUserType();

        RegisterRequest req = new RegisterRequest();
        req.setUserId(citizenInfo.getUserId());
        req.setCitizenid(citizenid);
        req.setBirthDate(request.getBirthDate());
        req.setEmail(request.getEmail());
        req.setFnameEN(request.getFnameEN());
        req.setFnameTH(request.getFnameTH());
        req.setLnameEN(request.getLnameEN());
        req.setLnameTH(request.getLnameTH());
        req.setMnameEN(request.getMnameEN());
        req.setMnameTH(request.getMnameTH());
        req.setMobile(request.getMobile());
        req.setMode("2");
        req.setUserType(citizenInfo.getUserType());
        req.setVerifiedType("1");

        if (citizenType.equals("2")) {
            req.setCompany_auth_id(request.getCompany_auth_id());
            req.setCompany_auth_titleNameTH(request.getCompany_auth_titleNameTH());
            req.setCompany_auth_fnameTH(request.getCompany_auth_fnameTH());
            req.setCompany_auth_lnameTH(request.getCompany_auth_lnameTH());
            req.setCompany_auth_titleNameEN(request.getCompany_auth_titleNameEN());
            req.setCompany_auth_fnameEN(request.getCompany_auth_fnameEN());
            req.setCompany_auth_lnameEN(request.getCompany_auth_lnameEN());
            req.setCompany_auth_mnameEN(request.getCompany_auth_mnameEN());
            req.setCompany_auth_mnamTH(request.getCompany_auth_mnamTH());
            req.setCompany_auth_email(request.getCompany_auth_email());
            req.setCompany_auth_birth_date(request.getCompany_auth_birth_date());
            req.setCompany_auth_phone(request.getCompany_auth_phone());
            req.setCompany_objective(citizenInfo.getCompany_objective());
            req.setCompany_status(citizenInfo.getCompany_status());

            AddressResponse workingAddress = request.getWorkingAddress();
            req.setMoo(workingAddress.getMoo());
            req.setNo(workingAddress.getNo());
            req.setPostCode(workingAddress.getPostCode());
            req.setProvinceId(workingAddress.getProvinceId());
            req.setRoad(workingAddress.getRoad());
            req.setSoi(workingAddress.getSoi());
            req.setTambonId(workingAddress.getTambonId());
            req.setAmphurId(workingAddress.getAmphurId());
            req.setVillage(workingAddress.getVillage());
        } else {
            AddressResponse billingAddress = request.getBillingAddress();
            req.setMoo(billingAddress.getMoo());
            req.setNo(billingAddress.getNo());
            req.setPostCode(billingAddress.getPostCode());
            req.setProvinceId(billingAddress.getProvinceId());
            req.setRoad(billingAddress.getRoad());
            req.setSoi(billingAddress.getSoi());
            req.setTambonId(billingAddress.getTambonId());
            req.setAmphurId(billingAddress.getAmphurId());
            req.setVillage(billingAddress.getVillage());
        }

        this.maintainAddress(citizenInfo, request.getBillingAddress(), "1");
        this.maintainAddress(citizenInfo, request.getWorkingAddress(), "2");

        if (citizenType.equals("1")) {
            this.maintainAddress(citizenInfo, request.getRegisterAddress(), "3");
        }

        String response = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).Register(req);
        if (!response.toLowerCase().contains("success"))
            return Response.status(Status.BAD_REQUEST).entity(response).build();
        else
            return Response.ok(response).build();

    }

    @GET
    @Path("/corporate/{corporateId}")
    @Produces("application/json")
    @Consumes("application/json")
    public Response GetCorporate(@PathParam("corporateId") String corporateId) {

        List<BusinessDetail> response = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE)
                .GetCorporate(corporateId);
        return Response.ok(response).build();
    }

    private void maintainAddress(Citizen citizen, AddressResponse address, String type) {
        if (address == null)
            return;

        address.setUserId(citizen.getUserId());
        address.setAddressType(type);

        ModelMapper modelMapper = new ModelMapper();

        if (address.getId() == null) { // new
            com.wisesoft.epayment.domain.Address r = modelMapper.map(address,
                    com.wisesoft.epayment.domain.Address.class);
            this.epaymentApiService().insertAddress(r);
        } else {
            com.wisesoft.epayment.domain.Address r = modelMapper.map(address,
                    com.wisesoft.epayment.domain.Address.class);
            this.epaymentApiService().updateAddress(r);
        }
    }

}