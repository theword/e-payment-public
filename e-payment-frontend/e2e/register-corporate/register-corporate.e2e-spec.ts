import { RegisterCorporatePage } from './register-corporate.po';


describe('Test Register Corporate page', () => {
    let page: RegisterCorporatePage;

    beforeEach(() => {
        page = new RegisterCorporatePage();
    });

    it('send id  1 expected Luke Skywalker', () => {
        page.navigateTo();
        page.setId(1);
        page.submit();

        expect(page.getResultText()).toEqual('Luke Skywalker');
    });

    it('send id  2 expected C-3PO', () => {
        page.navigateTo();
        page.setId(2);
        page.submit();

        expect(page.getResultText()).toEqual('C-3PO');
    });

});
