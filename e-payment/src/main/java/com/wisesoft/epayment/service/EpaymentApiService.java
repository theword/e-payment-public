package com.wisesoft.epayment.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Form;

import com.wisesoft.epayment.domain.District;
import com.wisesoft.epayment.domain.GetCitizenStatusResult;
import com.wisesoft.epayment.domain.GetReceiptResult;
import com.wisesoft.epayment.domain.Province;
import com.wisesoft.epayment.domain.RegisterCorporateRequest;
import com.wisesoft.epayment.domain.RegisterRequest;
import com.wisesoft.epayment.domain.SubDistrict;
import com.wisesoft.epayment.domain.Title;
import com.wisesoft.epayment.domain.Address;
import com.wisesoft.epayment.domain.BusinessDetail;
import com.wisesoft.epayment.domain.Citizen;

@Path("/epaymentApiService")
public interface EpaymentApiService {

    @POST
    @Produces("application/json")
    @Path("/getProvince")
    List<Province> getProvince();

    @POST
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    @Path("/getDistrict")
    List<District> getDistrict(Form form);

    @POST
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    @Path("/getSubDistrict")
    List<SubDistrict> getSubDistrict(Form form);

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/getTitleName")
    List<Title> getTitle(String request);

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/register")
    String Register(RegisterRequest obj);

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/register")
    String RegisterCorporate(RegisterCorporateRequest obj);

    @GET
    @Produces("application/json")
    @Path("/getUserByCitizen")
    Citizen GetCitizen(@QueryParam("citizenid") String citizenid);

    @GET
    @Produces("application/json")
    @Path("/getBusinessDeatail")
    List<BusinessDetail> GetCorporate(@QueryParam("businessCode") String businessCode);

    @GET
    @Produces("application/json")
    @Path("/forgetPassword")
    String forgotPassword(@QueryParam("citizenid") String citizenid, @QueryParam("email") String email);

    @GET
    @Produces("application/json")
    @Path("/searchAddress/{userId}")
    List<Address> searchAddress(@PathParam("userId") String userId);

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/updateAddress")
    String updateAddress(Address address);

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/insertAddress")
    String insertAddress(Address address);

    @GET
    @Produces("application/json")
    @Path("/deleteAddress/{id}")
    String deleteAddress(@PathParam("id") String id);

    @GET
    @Produces("application/json")
    @Path("/getStatus")
    GetCitizenStatusResult getCitizenStatus(@QueryParam("citizenid") String citizenid);

    @GET
    @Produces("application/json")
    @Path("/getReceipt")
    List<GetReceiptResult> getReceipt(@QueryParam("searchType") String searchType,
            @QueryParam("searchValue") String searchValue, @QueryParam("ref1") String ref1);

    @GET
    @Produces("application/json")
    @Path("/checkReceipt")
    boolean checkReceipt(@QueryParam("fileURL") String fileURL);
}