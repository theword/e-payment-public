import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as moment from 'moment';
import { environment } from '../../../environments/environment';
import * as faker from 'faker';
import { CaptchaComponent } from '../common/captcha.component';
import { ValidationService } from '../common/validation.service';
import { Router } from '@angular/router';
import { InputAddressComponent } from '../common/app-input-address.component';

@Component({
    selector: 'app-register-citizen',
    templateUrl: './register-citizen.component.html'
})

export class RegisterCitizenComponent implements OnInit {

    registerForm: FormGroup;
    agreement: any;
    provinces: Observable<any[]>;
    districts: Observable<any[]>;
    subdistricts: any[];
    titles: any[];
    inProgress: boolean;
    isRegisterComplete: boolean;
    @ViewChild('modalOTPRegister') modalOTPRegister;
    @ViewChild('modalRegisterComplete') modalRegisterComplete;
    @ViewChild(CaptchaComponent) recaptchaRef: CaptchaComponent;

    @ViewChild('billingAddress') billingAddress: InputAddressComponent;
    @ViewChild('registerAddress') registerAddress: InputAddressComponent;
    constructor(private http: HttpClient, private fb: FormBuilder, private vs: ValidationService, private router: Router) {
    }

    createForm() {
        this.registerForm = this.fb.group({
            citizenid: new FormControl(null, [Validators.required, this.vs.validatePersonNoFormat]),
            birthDate: new FormControl(null, [Validators.required]),
            titleNameTH: new FormControl(null, [Validators.required]),
            fnameTH: new FormControl(null, [Validators.required]),
            lnameTH: new FormControl(null, [Validators.required]),
            mnameTH: new FormControl(null),
            titleNameEN: new FormControl(null),
            fnameEN: new FormControl(null),
            lnameEN: new FormControl(null),
            mnameEN: new FormControl(),
            email: new FormControl(null, [Validators.required]),
            mobile: new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
            verifiedType: new FormControl('1', [Validators.required]),
            captcha: new FormControl(null, [Validators.required]),
            agreement: new FormControl(null, [Validators.required]),
            password: new FormControl(null, [Validators.required]),
            confirmPassword: new FormControl(null, [Validators.required]),
            billingAddress: this.billingAddress.address,
            registerAddress: this.registerAddress.address,
        }, { validator: [this.vs.passwordConfirming, this.vs.validatePassword] });

        this.registerForm.valueChanges.subscribe(v => {
            console.log(this.registerForm);
        });
    }

    ngOnInit() {
        this.createForm();
        this.http.get('api/common/titles').subscribe((r: any) => {
            this.titles = r;
            this.registerForm.controls['titleNameTH'].setValue(r[0].id);
            this.registerForm.controls['titleNameEN'].setValue(r[0].id);
        });

        this.http.get('api/posts/downloads/single/agreement').map((o: any) => {
            return {
                postId: o.id,
                content: o.content,
                fileName: (o.meta as any[]).find(i => i.key === 'ATTACH_NAME'),
                downloaded: (o.meta as any[]).find(i => i.key === 'page_view'),
                length: (o.meta as any[]).find(i => i.key === 'ATTACH_LENGTH'),
                fileId: (o.meta as any[]).find(i => i.key === 'attach_fileinfo_id'),
                createdDate: o.createdDate
            };
        }).subscribe(v => {
            this.agreement = v;
        });

    }
    onSubmit() {
        this.inProgress = true;
        this.http.post('api/register/citizen', this.registerForm.getRawValue()).subscribe((r: any) => {
            this.showDialog(true);
            this.inProgress = false;
        }, (err: any) => {
            this.showDialog(false);
            this.inProgress = false;
        });
    }

    revert() {
        this.registerForm.reset();
    }

    showDialog(registerComplete: boolean) {
        this.isRegisterComplete = registerComplete;
        if (this.registerForm.get('verifiedType').value === '1' && registerComplete) {
            this.showOTPDialog();
        } else {
            this.showRegisterDialog();
        }
    }

    showOTPDialog() {
        jQuery(this.modalOTPRegister.nativeElement).modal().show().on('hidden.bs.modal', () => {
            if (this.isRegisterComplete) {
                this.router.navigate(['/home']);
            } else {
                this.recaptchaRef.reset();
            }
        });
    }
    showRegisterDialog() {
        jQuery(this.modalRegisterComplete.nativeElement).modal().show().on('hidden.bs.modal', () => {
            if (this.isRegisterComplete) {
                this.router.navigate(['/home']);
            } else {
                this.recaptchaRef.reset();
            }
        });
    }

    copyAddress(event, source, destination): void {
        if (event.target.checked) {
            let v = source.value;

            if (destination.value.id == null)
                v.id = null;
            else
                v.id = destination.value.id;


            destination.setValue(v);
        }

    }


}
