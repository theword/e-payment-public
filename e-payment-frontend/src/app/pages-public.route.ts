import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterCorporateComponent } from './components/register-corporate/register-corporate.component';
import { RegisterCitizenComponent } from './components/register-citizen/register-citizen.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppCommonModule } from './components/common/app-common.module';
import { CommonModule } from '@angular/common';
import { BillingSearchComponent } from './components/billing/billing-search.component';
import { BillingValidateComponent } from './components/billing/billing-validate.component';
import { NewsDetailComponent } from './components/news/news-detail.component';
import { NewsListComponent } from './components/news/news-list.component';
import { DownloadGenericComponent } from './components/download/download-generic.component';
const routes: Routes = [
    { path: 'register-corporate', component: RegisterCorporateComponent },
    { path: 'register-citizen', component: RegisterCitizenComponent },
    {
        path: 'billing', children: [
            { path: 'search', component: BillingSearchComponent },
            { path: 'validate', component: BillingValidateComponent }
        ]
    },
    {
        path: 'news/:id', component: NewsDetailComponent
    },
    {
        path: 'news/type/:type', component: NewsListComponent
    },
    {
        path: 'download/:type', component: DownloadGenericComponent
    }
];

@NgModule({
    declarations: [
        RegisterCorporateComponent,
        RegisterCitizenComponent,
        BillingSearchComponent,
        BillingValidateComponent,
        NewsDetailComponent,
        NewsListComponent,
        DownloadGenericComponent
    ],
    imports: [AppCommonModule, CommonModule, HttpClientModule, ReactiveFormsModule, RouterModule.forChild(routes)],
    exports: [AppCommonModule, HttpClientModule, ReactiveFormsModule, RouterModule]
})

export class PagePublicRouteModule { }
