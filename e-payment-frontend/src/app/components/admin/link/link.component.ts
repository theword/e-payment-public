import { Component, OnInit, ViewChild, EventEmitter, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../../common/authentication.service';
import { Http } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataSource } from '../../common/data-source';
import { PaginationComponent } from '../../common/pagination';

@Component({
    selector: 'app-link',
    templateUrl: 'link.component.html',
    styleUrls: ['../admin.component.css']
})

export class LinkComponent implements OnInit {
    form: FormGroup;
    @ViewChild("file") fileBrowserInput: ElementRef;
    image: any;

    currentPostType: any;
    postTypeEmitter: EventEmitter<string> = new EventEmitter<string>()

    dataSource: DataSource<any>;
    rows: Observable<any>;
    nbLinkOrg: any;
    nbLink: any;

    @ViewChild(PaginationComponent) pagination: PaginationComponent;
    constructor(private auth: AuthenticationService) { }

    ngOnInit() {
        this.form = new FormGroup({
            title: new FormControl("", Validators.required),
            content: new FormControl("", Validators.required)
        });

        this.load();

        this.postType("LinkOrg");
    }

    load() {
        var allPosts = this.auth.get("api/posts/link");
        this.rows = allPosts;
        // allPosts.subscribe(v => {
        //     this.nbLinkOrg = v.filter(i => i.type == "LinkOrg").length;
        //     this.nbLink = v.filter(i => i.type == "Link").length;
        // });


        // this.dataSource = new PostsDataSource(allPosts, this.pagination, this.postTypeEmitter);
        // this.rows = this.dataSource.connect();
    }

    add() {
        var req = this.form.getRawValue();
        req.status = "Published";

        req.images = [this.image];
        req.postType = this.currentPostType;

        this.form.disable();
        this.auth.post(`api/cms/posts/0`, req).subscribe(v => {
            this.form.reset();
            this.image = undefined;
            this.form.enable();

            this.load();
            // this.postType(this.currentPostType);
        });


    }

    get canAdd() {
        return this.form.valid && this.image;
    }

    deletePost(post) {
        this.auth.delete("api/cms/posts/" + post.id).subscribe(v => {
            this.ngOnInit();
        });
    }

    addPicture() {
        this.fileBrowserInput.nativeElement.click();
    }

    postType(type) {
        this.currentPostType = type;
        this.postTypeEmitter.next(type);
    }

    fileBrowserChanged(event: any) {
        var file = event.target.files[0];
        if (file) {
            this.readFile(file, 0).subscribe(r => {
                this.image = r;
            });
        }
    }

    readFile(file, key): Observable<any> {
        return Observable.create(ob => {
            var reader = new FileReader();
            reader.onload = function (e: any) {
                var base64 = e.target.result;
                ob.next({
                    key: key,
                    data: base64,
                })
            }
            reader.readAsDataURL(file);
        });
    }

    getThumbnail(post) {
        let img = (post.meta as any[]).find(i => i.key == "attach_fileinfo_id");
        return img ? `files/posts/${img.id}.png` : undefined;
    }


}

class PostsDataSource extends DataSource<any> {

    _data: any[] = [];

    type: string;

    constructor(private data: Observable<any>, private paging: PaginationComponent, private postTypeEmitter: EventEmitter<string>) {
        super();

        this.data.subscribe(p => {
            this._data = p;
            this.paging.page = 1;
            this.paging.totalItems = this._data.length;
        });

        this.postTypeEmitter.subscribe(p => {
            this.type = p;
            this.paging.page = 1;
            this.paging.totalItems = this._data.length;
        });
    }

    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.data,
            this.paging.pageChanged,
            this.postTypeEmitter
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            var data = this._data.slice();
            data = data.filter(d => d.type == this.type);

            const startIndex = (this.paging.page - 1) * this.paging.itemsPerPage;
            return data.splice(startIndex, this.paging.itemsPerPage);
        });
    }
}
