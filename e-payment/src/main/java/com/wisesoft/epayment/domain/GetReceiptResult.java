package com.wisesoft.epayment.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown=true)
@Data
public class GetReceiptResult {
    private Date createdDate;
    private String billingNo;
    private String invoiceNo;
    private String billingId;
    private Invoice invoice;
    
    private double totalAmount;
    private Integer status;

    @Data
    @JsonIgnoreProperties(ignoreUnknown=true)
    private class Invoice{
        private String ref1;
        private String name;    
    }

}