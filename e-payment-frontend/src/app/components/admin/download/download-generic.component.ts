import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DialogComponent } from '../../common/app-dialog.component';
import { AuthenticationService } from '../../common/authentication.service';
import { HttpClient } from '@angular/common/http';

import { ActivatedRoute } from '@angular/router';
import { DownloadTableComponent } from '../../common/download/download-table.component';
import { LayoutComponent } from '../../common/layout.component';
@Component({
    selector: 'app-download-generic',
    templateUrl: 'download-generic.component.html',
    styleUrls: ['../admin.component.css']
})

export class DownloadGenericComponent implements OnInit {
    title: any;
    type: any;
    form: FormGroup;

    @ViewChild(DownloadTableComponent) table: DownloadTableComponent;
    @ViewChild('addDialog') addDialog: DialogComponent;
    @ViewChild(LayoutComponent) layout: LayoutComponent;
    constructor(private auth: AuthenticationService, private http: HttpClient, private route: ActivatedRoute) {

    }

    ngOnInit() {
        this.form = new FormGroup({
            name: new FormControl(null, Validators.required),
            file: new FormControl(null, Validators.required)
        });

        this.route.queryParamMap.subscribe(q => {
            this.title = q.get('title');
        });
        this.route.paramMap.subscribe(q => {
            this.type = q.get('type');
            this.table.type = this.type;
            this.table.load();
        });
    }

    openAdd() {
        this.form.reset();
        this.addDialog.open().subscribe(v => {

        });
    }

    onSubmit() {
        this.auth.post('api/cms/downloads/' + this.type, this.form.getRawValue()).subscribe(v => {
            const data = this.form.getRawValue();
            this.form.reset();
            this.addDialog.close();
            this.layout.alert(`เพิ่มเอกสาร ${data.name} สำเร็จ`);
            this.table.load();
        });
    }




}