import { Observable } from "rxjs/Observable";

export abstract class DataSource<T>{
    abstract connect(): Observable<any[]>;
}
