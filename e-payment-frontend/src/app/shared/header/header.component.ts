import { Router } from '@angular/router';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { NavigationEnd } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment.prod';
import { AuthenticationService } from '../../components/common/authentication.service';
import { ValidationService } from '../../components/common/validation.service';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DialogComponent } from '../../components/common/app-dialog.component';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

    @ViewChild('modalForgotPassword') forgotpasswordModal: ElementRef;
    @ViewChild('EditResultDialog') editResultDialog: DialogComponent;
    message: any;
    titleMessage: any;
    @ViewChild('loginResultDialog') loginResultDialog: DialogComponent;
    username: string;
    password: string;

    loginResult: string;

    constructor(private activatedRoute: ActivatedRoute, private auth: AuthenticationService, private routeParam: ActivatedRoute, private vs: ValidationService, private fb: FormBuilder, private http: HttpClient) {
        if (this.auth.hasToken()) {
            this.profile = jwt_decode(this.auth.getToken());
            this.profile = JSON.parse(this.profile.data);
        }

        this.forgetPassword = this.fb.group({
            citizenId: new FormControl(null, [Validators.required]),
            email: new FormControl(null, [Validators.required])
        });


        this.routeParam.queryParams.subscribe(q => {
            if (q.action && q.action === 'forgotpassword') {
                jQuery(this.forgotpasswordModal.nativeElement).modal();
            }
        });

    }
    isLoad = false;

    profile: any;

    forgetPassword: FormGroup;

    ngOnInit() {

    }

    canLogin() {
        return this.username.length === 13 && this.password;
    }

    count = 0;

    login() {
        this.isLoad = true;
        this.auth.authenticate(this.username, this.password).subscribe(v => {
            this.username = '';
            this.password = '';
            this.isLoad = false;
            window.location.href = 'home';
        }, er => {
            this.isLoad = false;
            this.loginResult = er.error;
            this.loginResultDialog.open();

            this.count++;

            if (this.count >= 3) {
                window.location.href = 'home?action=forgotpassword';
            }
        });

    }

    logout() {
        this.auth.removeToken();
        window.location.href = '';
    }


    clearForgotForm() {
        this.forgetPassword.reset();
        this.forgetPassword.get('email').reset();
    }

    forgot() {

        const win = window;

        let data = this.forgetPassword.getRawValue();
        this.clearForgotForm();
        this.http.post('api/register/forgotpassword', data).subscribe(s => {
            this.showDialog('แจ้งผลการทำงานของระบบ', 'ระบบได้ทำการส่งลิงค์เพื่อเปลี่ยนรหัสผ่านให้ท่านแล้ว, โปรดเช็คอีเมลของท่าน').subscribe(v => {
                win.location.href = '';
            });
        }, er => {
            this.showDialog('แจ้งข้อผิดพลาด', 'กรุณากรอกอีเมลที่ท่านลงทะเบียนไว้ในระบบ');
        });
    }

    showDialog(titleMessage, message): Observable<any> {
        this.titleMessage = titleMessage;
        this.message = message;
        const comp = this;
        return this.editResultDialog.open();
    }

}
