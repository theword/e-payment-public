package com.orange.epayment.data;

import org.springframework.data.repository.CrudRepository;
import com.orange.epayment.domain.PostMeta;
import java.util.List;

public interface PostMetaRepository extends CrudRepository<PostMeta, Long> {
    PostMeta getByPostIdAndKey(Long postId, String key);

    List<PostMeta> findByPostIdAndKey(Long postId, String key);
}
