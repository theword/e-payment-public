package com.orange.epayment.endpoint.response;

import lombok.Data;

@Data
public class BillingValidateResponse {
    private String fileURL;
    private Boolean result;
}