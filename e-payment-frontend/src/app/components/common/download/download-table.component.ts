import { Component, OnInit, Input, ViewChild, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../authentication.service';
import { LayoutComponent } from '../layout.component';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/merge';
import { DataSource } from '../data-source';
import { PaginationComponent } from '../pagination';

@Component({
    selector: 'app-download-table',
    templateUrl: 'download-table.component.html'
})

export class DownloadTableComponent implements OnInit {


    dataSource: DataSource<any>;
    rows: Observable<any>;

    @Input() type: any;
    @Input() deletable: any;
    @ViewChild(LayoutComponent) layout: LayoutComponent;
    @ViewChild(PaginationComponent) pagination: PaginationComponent;

    constructor(private auth: AuthenticationService, private http: HttpClient) { }

    ngOnInit() { }

    load() {

        const downloads = this.http.get('api/posts/downloads/' + this.type).map((r: any[]) => {
            return r.map(o => {
                return {
                    postId: o.id,
                    title: o.title,
                    fileName: (o.meta as any[]).find(i => i.key === 'ATTACH_NAME'),
                    downloaded: (o.meta as any[]).find(i => i.key === 'page_view'),
                    length: (o.meta as any[]).find(i => i.key === 'ATTACH_LENGTH'),
                    fileId: (o.meta as any[]).find(i => i.key === 'attach_fileinfo_id'),
                    createdDate: o.createdDate
                };
            });
        });

        this.dataSource = new DownloadDataSource(downloads, this.pagination, );

        this.rows = this.dataSource.connect();
    }


    getExtension(fileName) {
        return fileName.value.substr((fileName.value.lastIndexOf('.')));
    }

    filterTitle(title) {
        return "file";
    }


    delete(post) {
        this.layout.confirm(`ท่านต้องการลบ ${post.title} หรือไม่`).subscribe(v => {
            this.auth.delete('api/cms/downloads/' + this.type + '/' + post.postId).subscribe(v => {
                this.layout.alert('ลบข้อมูลเรียบร้อย');
                this.load();
            });
        });
    }
}



class DownloadDataSource extends DataSource<any> {

    _data: any[] = [];

    status: string;

    constructor(private data: Observable<any>, private paging: PaginationComponent) {
        super();

        this.data.subscribe(p => {
            this._data = p;
            this.paging.page = 1;
            this.paging.totalItems = this._data.length;
        });

    }

    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.data,
            this.paging.pageChanged
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this._data.slice();
            const startIndex = (this.paging.page - 1) * this.paging.itemsPerPage;
            return data.splice(startIndex, this.paging.itemsPerPage);
        });
    }
}

