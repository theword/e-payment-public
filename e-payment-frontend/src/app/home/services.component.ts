import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-services',
    templateUrl: 'services.component.html'
})

export class ServicesComponent implements OnInit, AfterViewInit {

    news: any[] = [];

    constructor(private http: HttpClient) {

    }

    ngOnInit() {
        this.http.get("api/posts/list/published/top3").subscribe((n: any[]) => {
            this.news = n;
        });
    }

    ngAfterViewInit(): void {

    }

    getThumbnail(post) {
        let img = (post.meta as any[]).find(i => i.key == "attach_fileinfo_id");
        return img ? `files/posts/${img.id}.png` : undefined;
    }

    openNews() {
        if (!this.news || this.news.length == 0)
            return;

        jQuery('.owl-carousel3').owlCarousel({
            margin: 60,
            pagination: false,
            navigation: true,
            items: 3,
            addClassActive: true,
            responsive: {
                0: {
                    items: 1
                },
                479: {
                    items: 1
                },
                768: {
                    items: 2
                },
                979: {
                    items: 3
                },
                1199: {
                    items: 3
                }
            }
        });
        jQuery('#news').slideDown('slow');
        jQuery('.more-btn').slideDown('slow');
        jQuery('.open-news-btn').hide('200');
        var owl = jQuery('.owl-carousel3').data('owlCarousel');
        owl.onResize();
    }
}
