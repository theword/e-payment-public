import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['../admin.component.css']
})

export class HomeComponent implements OnInit {

    menu = [
        {
            title: 'หน้าหลัก', icon: 'home',
            child: [
                { title: 'Banner หลัก', link: '้home-banner', icon: 'home' },
                { title: 'Banner Link', link: 'home-banner-link', icon: 'home' },
                { title: 'Link หน่วยงาน', link: 'link', icon: 'th' },
                { title: 'หมวดรายการรับชำระ', link: 'home-category', icon: 'home' },

            ]
        },
        { title: 'ข่าวสารและกิจกรรม', link: 'news', icon: 'globe' },
        {
            title: 'ดาวน์โหลด', icon: 'download-alt',
            child: [
                { title: 'แบบฟอร์มที่ให้ดาวน์โหลดได้', link: 'download/forms', icon: 'list-alt' },
                { title: 'กฎ ระเบียบ ข้อบังคับ ที่เกี่ยวข้องกับหน่วยงาน', link: 'download/agreement', icon: 'list-alt' },
                { title: 'ข้อมูลการให้บริการ/คู่มือ', link: 'download/manual', icon: 'list-alt' }

            ]
        },
        {
            title: 'ยอมรับเงื่อนไขในการใช้งานเว็บไซต์', link: 'list-alt'
        },
        {
            title: 'e-Form', link: 'default', icon: 'list-alt'
        }, {
            title: 'ผังเว็บไซต์', link: 'default', icon: 'equalizer'
        }, {
            title: 'ช่องทางการติดต่อสื่อสารกับผู้ใช้บริการ', link: 'default', icon: 'list-alt'
        }
        , {
            title: 'คำถามที่พบบ่อย(FAQ)', link: 'default', icon: 'comment'
        },
        {
            title: 'ถาม – ตอบ (Q&A)', link: 'default', icon: 'comment'
        },
        {
            title: 'เกี่ยวกับระบบชำระเงินกลาง ของบริการภาครัฐ', link: 'default', icon: 'info-sign'
        }

    ];

    constructor() { }

    ngOnInit() { }
}