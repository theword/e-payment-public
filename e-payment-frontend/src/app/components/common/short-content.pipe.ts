import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'shortcontent' })
export class ShortContentPipe implements PipeTransform {
    constructor() { }
    transform(value) {
        let v = value ? String(value).replace(/<[^>]+>/gm, '') : '';
        if (v.length > 240) {
            v = v.substring(0, 240) + '...';
        }
        return v;
    }
}
