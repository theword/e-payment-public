import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { InputAddressComponent } from '../../common/app-input-address.component';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { AuthenticationService } from '../../common/authentication.service';


@Component({
    selector: 'app-profile',
    templateUrl: 'profile.component.html'
})

export class ProfileComponent implements OnInit, AfterViewInit {

    decoded: any;
    constructor(private http: HttpClient, private fb: FormBuilder, private auth: AuthenticationService) {

    }

    ngOnInit() {
        this.decoded = jwt_decode(this.auth.getToken());
    }

    ngAfterViewInit(): void {

    }

}
