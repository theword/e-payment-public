package com.orange.epayment.endpoint.request;

import lombok.Data;

@Data
public class Address {
     private String no; 
     private String moo;
     private String soi; 
     private String road; 
     private String provinceId; 
     private String amphurId;
     private String tambonId;
     private String postCode;
     private String roomNo; 
}