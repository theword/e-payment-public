import { PipeTransform, Pipe } from '@angular/core';
import * as moment from 'moment';
@Pipe({ name: 'thaidate' })
export class ThaiDatePipe implements PipeTransform {
    constructor() { }
    transform(value) {
        moment.locale('th');
        const date = moment(value).add(543, 'years').format('DD MMMM YYYY');
        return date;
    }
}
