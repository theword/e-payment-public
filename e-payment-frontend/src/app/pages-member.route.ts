import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileCitizenComponent } from './components/member/profile-citizen/profile-citzen.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppCommonModule } from './components/common/app-common.module';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/member/home/home.component';
import { ProfileComponent } from './components/member/profile/profile.component';
import { MemberRouteGuard } from './components/member/member.routeguard';

import { ChangePasswordComponent } from './components/member/changepassword/changepassword.component';
import { ProfileCorporateComponent } from './components/member/profile-corporate/profile-corporate.component';
const routes: Routes = [
    {

        path: '', component: HomeComponent, canActivate: [MemberRouteGuard],
        children: [
            { path: '', redirectTo: 'profile' },
            { path: 'home', redirectTo: 'profile' },
            { path: 'profile', component: ProfileComponent },
            { path: 'changepassword', component: ChangePasswordComponent },
            { path: '**', redirectTo: 'profile' }
        ]
    }
];

@NgModule({
    declarations: [
        HomeComponent,
        ProfileComponent,
        ProfileCitizenComponent,
        ProfileCorporateComponent,
        ChangePasswordComponent
    ],
    imports: [AppCommonModule, CommonModule, HttpClientModule, ReactiveFormsModule, RouterModule.forChild(routes)],
    exports: [AppCommonModule, HttpClientModule, ReactiveFormsModule, RouterModule],
    providers: [
        MemberRouteGuard
    ]
})

export class PageMemberRouteModule { }
