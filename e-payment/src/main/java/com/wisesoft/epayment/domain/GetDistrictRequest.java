package com.wisesoft.epayment.domain;

import lombok.Data;

@Data
public class GetDistrictRequest {
    private int provinceId;
}