import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import 'rxjs/add/observable/merge';
import { DataSource } from '../common/data-source';
import { PaginationComponent } from '../common/pagination';
@Component({
    selector: 'app-news-list',
    templateUrl: 'news-list.component.html'
})

export class NewsListComponent implements OnInit, AfterViewInit {

    rows: Observable<any>;
    dataSource: DataSource<any>;
    @ViewChild(PaginationComponent) pagination: PaginationComponent;

    constructor(private http: HttpClient, private route: ActivatedRoute) {

    }

    ngOnInit() {
        this.route.params.subscribe(p => {
            let type = p.type;
            let news = this.http.get('api/posts/list/published').map((r: any[]) => {
                return r.filter(item => {
                    if (type != "all") {
                        let news_type = (item.meta as any[]).find(i => i.key === 'news_type');
                        return news_type.value === type;
                    } else {
                        return item;
                    }
                });
            });

            this.pagination.itemsPerPage = 5;
            this.dataSource = new NewsDataSource(news, this.pagination, );

            this.rows = this.dataSource.connect();
        });
    }

    getType(post) {
        if (!post.meta)
            return 0;

        let news_type = (post.meta as any[]).find(i => i.key === 'news_type');
        return news_type ? news_type.value : 0;

    }

    getThumbnail(post) {
        if (!post.meta)
            return null;
        let img = (post.meta as any[]).find(i => i.key == 'attach_fileinfo_id');
        return img ? `files/posts/${img.id}.png` : undefined;
    }

    ngAfterViewInit(): void {
        jQuery('.responsive-calendar').responsiveCalendar({

            time: '2018-01'
        });
        // jQuery('.responsive-calendar').responsiveCalendar({
        //     time: '2018-01',
        //     events: {
        //         '2018-01-01': { 'number': 5, 'url': 'http://w3widgets.com/responsive-slider' },
        //         '2018-01-26': { 'number': 1, 'url': 'http://w3widgets.com/responsive-slider' },
        //         '2018-01-12': { 'number': 1, 'url': 'http://w3widgets.com/responsive-slider' }
        //     }
        // });

    }
}


class NewsDataSource extends DataSource<any> {

    _data: any[] = [];

    status: string;

    constructor(private data: Observable<any>, private paging: PaginationComponent) {
        super();

        this.data.subscribe(p => {
            this._data = p;
            this.paging.page = 1;
            this.paging.totalItems = this._data.length;
        });

    }

    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.data,
            this.paging.pageChanged
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this._data.slice();
            const startIndex = (this.paging.page - 1) * this.paging.itemsPerPage;
            return data.splice(startIndex, this.paging.itemsPerPage);
        });
    }
}