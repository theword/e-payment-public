import { Component, OnInit } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { AuthenticationService } from '../../common/authentication.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})

export class HomeComponent implements OnInit, AfterViewInit {
    @ViewChild('menu') menu: ElementRef;
    constructor(private auth: AuthenticationService) {

    }
    ngOnInit() { }

    ngAfterViewInit(): void {

    }
    logout() {
        this.auth.removeToken();
        window.location.href = '';
    }
}
