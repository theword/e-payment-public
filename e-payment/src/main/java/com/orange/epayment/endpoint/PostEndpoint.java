package com.orange.epayment.endpoint;

import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

import com.orange.epayment.data.PostMetaRepository;
import com.orange.epayment.data.PostRepository;
import com.orange.epayment.domain.Post;
import com.orange.epayment.domain.PostDetail;
import com.orange.epayment.domain.PostInfo;
import com.orange.epayment.domain.PostMeta;
import com.orange.epayment.domain.PostStatus;
import com.orange.epayment.domain.PostType;
import java.util.Collection;
import javax.ws.rs.core.MediaType;

@Component
@Path("/posts")
public class PostEndpoint {

    private PostRepository postRepo;
    private PostMetaRepository postMetaRepo;

    @Autowired
    public PostEndpoint(PostRepository postRepository, PostMetaRepository postMetaRepo) {
        this.postRepo = postRepository;
        this.postMetaRepo = postMetaRepo;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Transactional
    public Response getPostById(@PathParam("id") Long postId) {

        PostDetail post = this.postRepo.findById(postId);

        return Response.ok(post).build();
    }

    @GET
    @Path("/list/published/top3")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPublishedTop5() {

        Collection<PostInfo> postInfo = this.postRepo
                .findTop3ByStatusAndTypeOrderByCreatedDateDesc(PostStatus.Published, PostType.Post);

        return Response.ok(postInfo).build();
    }

    @GET
    @Path("{id}/withcount")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Transactional
    public Response getPostByIdWithCount(@PathParam("id") Long postId) {

        PostMeta meta = this.postMetaRepo.getByPostIdAndKey(postId, PostMeta.PAGE_VIEW);
        if (meta != null) {
            meta.setValue(String.valueOf(Integer.valueOf(meta.getValue()) + 1));
            this.postMetaRepo.save(meta);
        }

        PostDetail post = this.postRepo.findById(postId);

        return Response.ok(post).build();
    }

    @GET
    @Path("/list/published")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPublished() {

        Collection<PostInfo> postInfo = this.postRepo.findByStatusAndTypeOrderByCreatedDateDesc(PostStatus.Published,
                PostType.Post);

        return Response.ok(postInfo).build();
    }

    @GET
    @Path("/downloads/{type}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDownloadByType(@PathParam("type") String type) {

        Collection<PostInfo> postInfo = this.postRepo.findByStatusAndTypeAndMetaKeyAndMetaValueOrderByCreatedDateDesc(
                PostStatus.Published, PostType.Attachment, PostMeta.ATTACH_TYPE, type);

        return Response.ok(postInfo).build();
    }

    @GET
    @Path("/downloads/single/{type}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDownloadSingleByType(@PathParam("type") String type) {

        PostInfo postInfo = this.postRepo.findTop1ByStatusAndTypeAndMetaKeyAndMetaValueOrderByCreatedDateDesc(
                PostStatus.Published, PostType.Attachment, PostMeta.ATTACH_TYPE, type);

        return Response.ok(postInfo).build();
    }

    @GET
    @Path("/link")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getLinks() {

        Collection<PostDetail> link = this.postRepo
                .findDetailByStatusAndTypeOrderByCreatedDateDesc(PostStatus.Published, PostType.Link);

        Collection<PostDetail> linkOrg = this.postRepo
                .findDetailByStatusAndTypeOrderByCreatedDateDesc(PostStatus.Published, PostType.LinkOrg);

        link.addAll(linkOrg);

        return Response.ok(link).build();
    }
}