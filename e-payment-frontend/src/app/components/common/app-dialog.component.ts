import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Component({
    selector: 'app-dialog',
    templateUrl: 'app-dialog.component.html'
})

export class DialogComponent implements OnInit {

    @Input() title: any;
    @ViewChild('dialog') dialogEle: ElementRef;

    constructor() { }

    ngOnInit() { }

    open(): Observable<any> {
        const subject = new Subject();
        jQuery(this.dialogEle.nativeElement).modal({
            backdrop: 'static',
            keyboard: false
        }).show().on('hidden.bs.modal', () => {
            subject.next(null);
            subject.complete();
        });

        return subject.asObservable();
    }

    close() {
        jQuery(this.dialogEle.nativeElement).modal('hide');
    }
}
