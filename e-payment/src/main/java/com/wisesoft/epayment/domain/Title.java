package com.wisesoft.epayment.domain;

import lombok.Data;

@Data
public class Title {
    private int id;
    private String desc;
    private String desc2;
    private String desc3;
}