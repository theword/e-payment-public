import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../common/authentication.service';
import { CaptchaComponent } from '../common/captcha.component';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from '../common/validation.service';
import { debounce } from 'rxjs/operator/debounce';
import { Observable } from 'rxjs/Observable';
import { DialogComponent } from '../common/app-dialog.component';

@Component({
    selector: 'app-resetpassword',
    templateUrl: 'resetpassword.component.html'
})

export class ResetpasswordComponent implements OnInit {

    @ViewChild('EditResultDialog') editResultDialog: DialogComponent;
    message: any;
    form: FormGroup;
    inProgress: boolean;
    @ViewChild(CaptchaComponent) captcha: CaptchaComponent;

    currentToken: any;
    titleMessage: any;
    constructor(private route: ActivatedRoute, private http: HttpClient, private auth: AuthenticationService, private fb: FormBuilder, private vs: ValidationService) {
        this.form = this.fb.group({
            fnameEN: new FormControl(null),
            lnameEN: new FormControl(null),
            password: new FormControl('', [Validators.required]),
            confirmPassword: new FormControl('', Validators.required),
            captcha: new FormControl('', Validators.required)
        }, { validator: [this.vs.passwordConfirming, this.vs.validatePassword] });
    }

    ngOnInit() {

        this.route.queryParams.subscribe(q => {
            if (!q.citizenid)
                window.location.href = '';
            else {
                this.auth.authenticateWithToken(q.citizenid).subscribe(r => {
                    this.currentToken = r;

                    const decoded = jwt_decode(this.currentToken.access_token);
                    const data = JSON.parse(decoded.data);
                    this.form.get('fnameEN').setValue(data.fnameEN);
                    this.form.get('lnameEN').setValue(data.lnameEN);
                }, er => {
                    window.location.href = '';
                });
            }
        });
    }

    submit() {
        let win = window;
        this.inProgress = true;
        const data = this.form.getRawValue();

        this.form.reset();
        this.captcha.reset();
        this.auth.postWithToken('api/member/me/changepassword', this.currentToken.access_token, data).subscribe(vv => {

            this.showDialog('แจ้งผลการทำงานของระบบ', 'เปลี่ยนรหัสผ่านสำเร็จ').subscribe(v => {
                win.location.href = '';
            });
            this.inProgress = false;
        }, er => {
            this.showDialog('แจ้งข้อผิดพลาด', 'เปลี่ยนรหัสผ่านไม่สำเร็จ, โปรดลองอีกครั้ง');
            this.inProgress = false;
        });
    }


    cancel() {
        this.form.reset();
        this.captcha.reset();
    }

    showDialog(titleMessage, message): Observable<any> {
        this.titleMessage = titleMessage;
        this.message = message;
        const comp = this;
        return this.editResultDialog.open();
    }
}
