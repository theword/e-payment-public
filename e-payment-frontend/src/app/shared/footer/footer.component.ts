import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-footer',
    templateUrl: 'footer.component.html'
})

export class FooterComponent implements OnInit {

    externalAdminUrl = '';

    constructor(private http: HttpClient) {
        this.http.get('api/common/env').subscribe((v: any) => {
            this.externalAdminUrl = v.externalAdminUrl;
        });
    }

    ngOnInit() { }
}
