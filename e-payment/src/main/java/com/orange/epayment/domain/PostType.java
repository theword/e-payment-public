package com.orange.epayment.domain;

public enum PostType {
    Post, Page, Attachment, Link, LinkOrg
}
