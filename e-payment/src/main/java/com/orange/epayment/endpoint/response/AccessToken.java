package com.orange.epayment.endpoint.response;

import java.io.Serializable;

import lombok.Data;

@Data
public class AccessToken implements Serializable {

	private String access_token;
}
