import { Directive, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
    selector: '[appAccordian]',
})
export class AppAccordionDirective implements AfterViewInit {
    ngAfterViewInit(): void {

        const comp = this;
        jQuery(comp.el.nativeElement).accordion({
            active: 0,
            // collapsible: true,
            autoHeight: true,
            heightStyle: 'content'
        });
    }


    constructor(private el: ElementRef) {

    }


}
