package com.wisesoft.epayment.domain;

import lombok.Data;

@Data
public class LoginRequest {
    private String username;
    private String password;
    private String clientSecret;
}