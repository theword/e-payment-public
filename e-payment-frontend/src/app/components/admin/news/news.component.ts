import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

import 'rxjs/add/observable/merge';
import { AuthenticationService } from '../../common/authentication.service';
import { Observable } from 'rxjs';
import { DataSource } from '../../common/data-source';
import { PaginationComponent } from '../../common/pagination';

@Component({
    selector: 'app-news',
    templateUrl: 'news.component.html',
    styleUrls: ['../admin.component.css']
})
export class NewsComponent implements OnInit {

    postStatusEmittor: EventEmitter<string> = new EventEmitter<string>()

    dataSource: DataSource<any>;
    rows: Observable<any>;


    @ViewChild(PaginationComponent) pagination: PaginationComponent;

    content: string;
    token: string;
    post: any;
    status: any;

    constructor(private route: ActivatedRoute, private auth: AuthenticationService) {

    }

    nbAll = 0;
    nbPublished = 0;
    nbDraft = 0;

    ngOnInit(): void {

        let allPosts = this.auth.get('api/cms/posts/all');

        allPosts.subscribe(v => {
            this.nbAll = v.length;
            this.nbPublished = v.filter(i => i.status === 'Published').length;
            this.nbDraft = v.filter(i => i.status === 'Draft').length;
        });


        this.dataSource = new PostsDataSource(allPosts, this.pagination, this.postStatusEmittor);
        this.postStatus('All');
        this.rows = this.dataSource.connect();
    }


    delete(post: any) {
        if (confirm(`ลบ '${post.title}' ?`)) {
            this.auth.delete('api/cms/posts/' + post.id).subscribe(v => {
                this.ngOnInit();
            });
        }
    }


    getPageView(post) {
        let pageView = (post.meta as any[]).find(i => i.key === 'page_view');
        return pageView ? pageView.value : 0;
    }

    getType(post) {
        let news_type = (post.meta as any[]).find(i => i.key === 'news_type');
        return news_type ? news_type.value : 0;

    }

    postStatus(status) {
        this.status = status;
        this.postStatusEmittor.next(status);
    }
}

class PostsDataSource extends DataSource<any> {

    _data: any[] = [];

    status: string;

    constructor(private data: Observable<any>, private paging: PaginationComponent, private postStatusEmittor: EventEmitter<string>) {
        super();

        this.data.subscribe(p => {
            this._data = p;
            this.paging.page = 1;
            this.paging.totalItems = this._data.length;
        });

        this.postStatusEmittor.subscribe(p => {
            this.status = p;
            this.paging.page = 1;
            this.paging.totalItems = this._data.length;
        });
    }

    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.data,
            this.paging.pageChanged,
            this.postStatusEmittor
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this._data.slice();
            if (this.status != "All")
                data = data.filter(d => d.status == this.status);

            const startIndex = (this.paging.page - 1) * this.paging.itemsPerPage;
            return data.splice(startIndex, this.paging.itemsPerPage);
        });
    }
}
