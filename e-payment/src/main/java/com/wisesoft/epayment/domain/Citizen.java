package com.wisesoft.epayment.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Citizen {

  private String username;
  private String password;
  private String email;
  private String citizenid;
  private String mobile;
  private String fnameEN;
  private String fnameTH;
  private String sex;
  private String userType;
  private String birthDate;
  private String birthDateStr;
  private String verifiedType;
  private String titleNameTH;
  private String titleNameEN;
  private String mnameEN;
  private String mnameTH;
  private String lnameEN;
  private String lnameTH;
  private String age;
  private String company_status;
  private String company_objective;
  private String company_auth_id;
  private String company_auth_birth_date;
  private String company_auth_email;
  private String company_auth_phone;
  private String company_auth_doc;
  private String address_id;
  private String orgType;
  private String orgSubType;
  private String dept;
  private String bizArea;
  private String cosCenter;
  private String department;
  private String building_name;
  private String moo;
  private String no;
  private String road;
  private String soi;
  private String village;
  private String postCode;
  private String roomNo;
  private String provinceId;
  private String provinceName;
  private String amphurId;
  private String amphurName;
  private String tambonId;
  private String tambonName;
  private String captcha;
  private String permissionGroup;
  private String mode;
  private String userId;

  private String company_auth_titleNameTH;
  private String company_auth_fnameTH;
  private String company_auth_lnameTH;
  private String company_auth_mnamTH;
  private String company_auth_titleNameEN;
  private String company_auth_fnameEN;
  private String company_auth_lnameEN;
  private String company_auth_mnameEN;
}