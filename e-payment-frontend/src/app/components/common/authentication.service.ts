import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { locales } from 'moment';
@Injectable()
export class AuthenticationService extends HttpClient {

    tokenName = 'epayment.token';

    get(url): Observable<any> {
        return super.get(url, { headers: this.getHeader(null) });
    }

    delete(url): Observable<any> {
        return super.delete(url, { headers: this.getHeader(null) });
    }

    post(url, data: any): Observable<any> {
        return super.post(url, data, { headers: this.getHeader(null) });
    }



    postWithToken(url, token, data: any): Observable<any> {
        return super.post(url, data, { headers: this.getHeader(token) });
    }


    private getHeader(preferedToken): HttpHeaders {
        let headers = new HttpHeaders();
        let token = this.getToken();
        if (preferedToken)
            token = preferedToken;

        if (token) {
            headers = new HttpHeaders({
                'Authorization': 'Bearer ' + token
            });
        }

        console.log(headers);

        return headers;
    }

    hasToken() {
        return this.getToken();
    }

    getToken() {
        if (window.localStorage.getItem(this.tokenName)) {
            const token = JSON.parse(window.localStorage.getItem(this.tokenName));
            return token.access_token;
        } else
            return null;
    }

    authenticate(username, password): Observable<any> {
        const auth = super.post('api/auth/token', { username: username, password: password });

        auth.subscribe(v => {
            localStorage.setItem(this.tokenName, JSON.stringify(v));
        });

        return auth;
    }

    authenticateWithToken(token): Observable<any> {
        const auth = super.post('api/auth/token', { token: token });

        return auth;
    }


    removeToken() {
        localStorage.removeItem(this.tokenName);
    }



}
