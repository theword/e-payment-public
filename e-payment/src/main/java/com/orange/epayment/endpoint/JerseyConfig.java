package com.orange.epayment.endpoint;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;

import lombok.extern.java.Log;

@Log
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {

        register(new LoggingFeature(log));

        register(CommonEndpoint.class);
        register(PostEndpoint.class);
        register(RegisterEndpoint.class);
        register(AuthenticationEndpoint.class);
        register(MemberEndpoint.class);
        register(AuthenticationFilter.class);
        register(BillingEndpoint.class);
        register(CMSEndpoint.class);
    }
}