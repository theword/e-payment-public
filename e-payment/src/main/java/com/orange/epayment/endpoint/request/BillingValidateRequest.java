package com.orange.epayment.endpoint.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class BillingValidateRequest {
    private String file;
}