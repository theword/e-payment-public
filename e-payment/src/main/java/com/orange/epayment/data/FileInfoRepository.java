package com.orange.epayment.data;

import org.springframework.data.repository.CrudRepository;

import com.orange.epayment.domain.FileInfo;

public interface FileInfoRepository extends CrudRepository<FileInfo, Long> {
    FileInfo getByFileNameAndId(String fileName, Long id);
}
