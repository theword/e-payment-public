import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WyswygeditorComponent } from './wyswygeditor.component';

describe('WyswygeditorComponent', () => {
  let component: WyswygeditorComponent;
  let fixture: ComponentFixture<WyswygeditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WyswygeditorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WyswygeditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
