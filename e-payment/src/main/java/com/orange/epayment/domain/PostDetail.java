package com.orange.epayment.domain;

public interface PostDetail extends PostInfo {
    String getContent();
}