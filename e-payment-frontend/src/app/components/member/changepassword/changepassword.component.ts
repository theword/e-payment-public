import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { CaptchaComponent } from '../../common/captcha.component';
import { HttpClient } from '@angular/common/http';
import { ValidationService } from '../../common/validation.service';
import { FormBuilder } from '@angular/forms';
import { AuthenticationService } from '../../common/authentication.service';
import { DialogComponent } from '../../common/app-dialog.component';

@Component({
    selector: 'app-profile-changepassword',
    templateUrl: 'changepassword.component.html'
})

export class ChangePasswordComponent implements OnInit {
    @ViewChild('EditResultDialog') editResultDialog: DialogComponent;
    message: any;
    titleMessage: any;
    form: FormGroup;
    inProgress: boolean;
    @ViewChild(CaptchaComponent) captcha: CaptchaComponent;
    constructor(private http: HttpClient, private fb: FormBuilder, private vs: ValidationService, private auth: AuthenticationService) {
        this.form = this.fb.group({
            fnameEN: new FormControl(null),
            lnameEN: new FormControl(null),
            currentpassword: new FormControl('', Validators.required),
            password: new FormControl('', [Validators.required]),
            confirmPassword: new FormControl('', Validators.required),
            captcha: new FormControl('', Validators.required)
        }, { validator: [this.vs.passwordConfirming, this.vs.validatePassword] });
    }

    ngOnInit() {
    }

    cancel() {
        this.form.reset();
        this.captcha.reset();
    }

    submit() {
        const decoded = jwt_decode(this.auth.getToken());
        this.inProgress = true;
        const data = this.form.getRawValue();
        this.http.post('api/auth/token', { username: decoded.sub, password: data.currentpassword }).subscribe(v => {
            this.form.reset();
            this.captcha.reset();
            this.auth.post('api/member/me/changepassword', data).subscribe(vv => {
                this.showDialog('แจ้งผลการทำงานของระบบ', 'เปลี่ยนรหัสผ่านสำเร็จ');
                this.inProgress = false;
            }, er => {
                this.showDialog('แจ้งข้อผิดพลาด', 'เปลี่ยนรหัสผ่านไม่สำเร็จ, โปรดลองอีกครั้ง');
                this.inProgress = false;
            });
        }, er => {
            this.showDialog('แจ้งข้อผิดพลาด', 'รหัสผ่านเดิมไม่ถูกต้อง');
            this.inProgress = false;
        });
    }

    showDialog(titleMessage, message) {
        this.titleMessage = titleMessage;
        this.message = message;
        const comp = this;
        this.editResultDialog.open().subscribe(v => {

        });
    }
}
