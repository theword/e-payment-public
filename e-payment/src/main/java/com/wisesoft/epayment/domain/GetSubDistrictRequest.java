package com.wisesoft.epayment.domain;

import lombok.Data;

@Data
public class GetSubDistrictRequest {
    private int provinceId;
}