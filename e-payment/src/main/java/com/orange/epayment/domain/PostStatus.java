package com.orange.epayment.domain;

public enum PostStatus {

    Draft, Published

}
