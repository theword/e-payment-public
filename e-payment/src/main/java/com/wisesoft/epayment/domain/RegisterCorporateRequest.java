package com.wisesoft.epayment.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class RegisterCorporateRequest extends RegisterRequest {
    private String company_auth_fnameTH;
    private String company_auth_lnameTH;
    private String company_auth_mnamTH;
    private String company_auth_titleNameEN;
    private String company_auth_fnameEN;
    private String company_auth_lnameEN;
    private String company_auth_mnameEN;
    private String company_auth_titleNameTH;
}