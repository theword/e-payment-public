package com.orange.epayment.endpoint;

import java.io.UnsupportedEncodingException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.impl.PublicClaims;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.epayment.AppEnvironment;
import com.orange.epayment.endpoint.request.Credentials;
import com.orange.epayment.endpoint.response.AccessToken;

import com.wisesoft.epayment.domain.Citizen;
import com.wisesoft.epayment.domain.GetCitizenStatusResult;
import com.wisesoft.epayment.service.EpaymentApiService;
import com.wisesoft.epayment.service.WisesoftRootService;

import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.UriBuilder;

import lombok.extern.slf4j.Slf4j;

@Component
@Path("/auth")
		@ Slf4j
public class AuthenticationEndpoint {

	public static final String SECRET = "N3QdXiwg67K0TrrJiy71EgXn4h0tHcNN";
	public static final String ISSUER = "e-payment";
	public static final int TOKEN_EXPIRES_IN_MINUTES = 30;

	private BeanFactory beanFactory;
	private AppEnvironment env;

	@Autowired
	public AuthenticationEndpoint(BeanFactory beanFactory, AppEnvironment env) {
		this.beanFactory = beanFactory;
		this.env = env;
	}

	private EpaymentApiService epaymentApiService(MediaType contentType) {
		return this.beanFactory.getBean(EpaymentApiService.class, contentType);
	}

	private WisesoftRootService wisesoftRootService(MediaType contentType) {
		return this.beanFactory.getBean(WisesoftRootService.class, contentType);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/token")
	public Response authenticateUser(com.orange.epayment.endpoint.request.Credentials credentials) {

		try {

			AccessToken res = new AccessToken();

			if (credentials.getToken() != null) {
				String plain = new String(Base64.getDecoder().decode(credentials.getToken()));
				log.info(plain);
				String[] segments = plain.split("\\|");
				if (segments[1].equals("5a0a6a5d3dbe62171ce59841")) {
					String token = issueToken(segments[0], false);
					res.setAccess_token(token);
				}
			} else {
				String username = credentials.getUsername();
				String password = credentials.getPassword();
				// Authenticate the user using the credentials provided
				authenticate(username, password);
				// Issue a token for the user
				String token = issueToken(username, false);
				res.setAccess_token(token);
			}

			// Return the token on the response
			return Response.ok(res).build();

		} catch (Exception e) {
			log.error("{}", e);
			return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
		}
	}

	// @POST
	// @Produces(MediaType.APPLICATION_JSON)
	// @Consumes(MediaType.APPLICATION_JSON)
	// @Path("token")
	public Response authorizeUser(Credentials credentials) {

		try {

			String username = credentials.getUsername();

			// Authenticate the user using the credentials provided
			if (!credentials.getClientSecret().equals(AuthenticationEndpoint.SECRET))
				throw new Exception("client secret is incorrect");

			// Issue a token for the user
			String token = issueToken(username, true);

			AccessToken res = new AccessToken();
			res.setAccess_token(token);

			// Return the token on the response
			return Response.ok(res).build();

		} catch (Exception e) {
			log.error("{}", e);
			return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
		}
	}

	private void authenticate(String username, String password) throws Exception {

		List<javax.ws.rs.core.Cookie> cookies = null;

		try {

			GetCitizenStatusResult result = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE)
					.getCitizenStatus(username);

			if (!result.getCode().equals("3"))
				throw new NotFoundException(result.getCode());

			ClientConfig config = new ClientConfig().connectorProvider(new ApacheConnectorProvider());
			Client client = ClientBuilder.newClient(config);
			Form form = new Form();
			form.param("username", username);
			form.param("password", password);
			form.param("clientSecret", "5a0a6a5d3dbe62171ce59841");
			WebTarget target = client.target(this.env.getWisesoftApiBaseUrl()).path("/login");
			Response response = target.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.form(form));

			cookies = new ArrayList<javax.ws.rs.core.Cookie>(response.getCookies().values());
			String user = this.getUser(cookies, username);
			log.info(user);
		} catch (Exception e) {
			log.error("{0}", e);
			throw e;
		}
	}

	public String getUser(List<Cookie> cookies, String username) throws Exception {
		String userId = null;
		try {

			ClientConfig config = new ClientConfig().connectorProvider(new ApacheConnectorProvider());

			Client client = ClientBuilder.newClient(config);

			WebTarget target = client.target(this.env.getWisesoftApiBaseUrl()).path("/loginApi");
			Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
			for (Cookie c : cookies) {
				invocationBuilder = invocationBuilder.cookie(c);
			}
			Response response = invocationBuilder.get();

			String responseString = response.readEntity(String.class);
			if (responseString.contains("<html"))
				throw new NotFoundException(username);

			userId = responseString;

		} catch (Exception e) {
			log.error("{0}", e);
			throw new NotFoundException(username);
		}
		return userId;
	}

	private String issueToken(String username, boolean isExternal) throws IllegalArgumentException,
			UnsupportedEncodingException, JWTCreationException, ParseException, JsonProcessingException {

		Citizen citizen = this.epaymentApiService(MediaType.APPLICATION_JSON_TYPE).GetCitizen(username);
		citizen.setPassword("");

		Date date = new Date();
		long t = date.getTime();
		Date expirationTime = new Date(t + TOKEN_EXPIRES_IN_MINUTES * 60 * 10000); // set 5 seconds
		Algorithm algorithm = Algorithm.HMAC256(AuthenticationEndpoint.SECRET);
		Builder tokenBuilder = JWT.create().withIssuer(AuthenticationEndpoint.ISSUER)
				.withClaim(PublicClaims.SUBJECT, username).withExpiresAt(expirationTime)
				.withClaim(PublicClaims.TYPE, citizen.getUserType())
				.withClaim("data", new ObjectMapper().writeValueAsString(citizen));

		if (isExternal) {
			tokenBuilder.withClaim("isExternal", isExternal);
		}

		String token = tokenBuilder.sign(algorithm);
		return token;

	}
}
