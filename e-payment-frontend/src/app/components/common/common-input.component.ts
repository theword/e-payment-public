import { Component, OnInit, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { forwardRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { NgModel } from '@angular/forms';
import { NG_VALIDATORS } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms/src/model';
import { ControlValueAccessor } from '@angular/forms';
import { OnChanges } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { ElementRef } from '@angular/core';



export function validateEmail(c: FormControl) {
    if (c.value != null && c.value !== '') {
        const pattern = new RegExp(/^.+@.+\..+$/);
        const result = pattern.test(c.value);
        return result ? null : { pattern: true };
    } else {
        return null;
    }
}


@Component({
    selector: 'app-email-input',
    template: `
        <input type="text" [placeholder]="placeholder" (change)="onInputChange($event.target.value)" #input>
    `,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => EmailInputComponent),
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useValue: validateEmail,
            multi: true
        }
    ]
})

export class EmailInputComponent implements AfterViewInit, ControlValueAccessor {
    _value = null;

    @ViewChild('input') input: ElementRef;

    @Input() placeholder = '';

    onChange: any = () => { };
    onTouched: any = () => { };
    constructor() {

    }

    onInputChange(val) {
        this.value = val;
    }

    ngAfterViewInit() {

    }

    get value() {
        return this._value;
    }

    set value(val: any) {
        this._value = val;
        this.input.nativeElement.value = val;
        this.onChange(val);
        this.onTouched();
    }

    registerOnChange(fn) {
        this.onChange = fn;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }

    writeValue(value) {
        if (value) {
            this.value = value;
        }
    }

}
