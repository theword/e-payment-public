package com.orange.epayment.data;

import org.springframework.data.repository.CrudRepository;
import com.orange.epayment.domain.Post;
import com.orange.epayment.domain.PostDetail;
import com.orange.epayment.domain.PostInfo;
import com.orange.epayment.domain.PostMeta;
import com.orange.epayment.domain.PostStatus;
import com.orange.epayment.domain.PostType;

import java.util.List;

public interface PostRepository extends CrudRepository<Post, Long> {
    List<PostInfo> findByStatusAndTypeOrderByCreatedDateDesc(PostStatus postStatus, PostType type);

    List<PostInfo> findByStatusAndTypeAndMetaKeyAndMetaValueOrderByCreatedDateDesc(PostStatus postStatus, PostType type,
            String metaKey, String metaValue);

    PostInfo findTop1ByStatusAndTypeAndMetaKeyAndMetaValueOrderByCreatedDateDesc(PostStatus postStatus, PostType type,
            String metaKey, String metaValue);

    List<PostDetail> findDetailByStatusAndTypeOrderByCreatedDateDesc(PostStatus postStatus, PostType type);

    List<PostInfo> findTop3ByStatusAndTypeOrderByCreatedDateDesc(PostStatus postStatus, PostType type);

    List<PostInfo> findByTypeOrderByCreatedDateDesc(PostType type);

    PostDetail findById(Long id);
}