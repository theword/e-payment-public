import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ViewChild } from '@angular/core';
import { CaptchaComponent } from '../common/captcha.component';

@Component({
    selector: 'app-billing-validate',
    templateUrl: './billing-validate.component.html'
})
export class BillingValidateComponent implements OnInit {
    form: FormGroup;
    loading = false;
    result: any;
    @ViewChild('captcha') captcha: CaptchaComponent;
    constructor(private http: HttpClient) { }

    ngOnInit() {
        this.form = new FormGroup({
            file: new FormControl(null, Validators.required),
            captcha: new FormControl(null, Validators.required)
        });

        this.form.valueChanges.subscribe(v => {
            console.log(v);
        });
    }

    submit() {
        this.result = undefined;
        this.loading = true;
        this.captcha.reset();
        this.http.post('api/billings/validate', this.form.getRawValue()).subscribe(v => {
            this.loading = false;
            this.result = v;
        }, er => {
            this.loading = false;
        });
    }
}
