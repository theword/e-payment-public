package com.orange.epayment.endpoint.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties
@Data
public class EditCorporateRequest {
    
     private String birthDate;
     private String titleNameTH ;
     private String fnameTH;
     private String lnameTH; 
     private String mnameTH;
     private String titleNameEN;
     private String fnameEN; 
     private String lnameEN; 
     private String mnameEN; 
     private String email; 
     private String mobile;
     private String captcha;
     private String age;
     private Address address1;
     private Address address2;
     private String file;
     private String statusText;
     private String citizenId;
}
