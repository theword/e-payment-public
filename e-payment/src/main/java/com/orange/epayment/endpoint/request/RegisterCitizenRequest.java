package com.orange.epayment.endpoint.request;

import com.wisesoft.epayment.domain.Address;

import lombok.Data;

@Data
public class RegisterCitizenRequest {

    private String citizenid;
    private String birthDate;
    private String titleNameTH;
    private String fnameTH;
    private String lnameTH;
    private String mnameTH;
    private String titleNameEN;
    private String fnameEN;
    private String lnameEN;
    private String mnameEN;
    private String email;
    private String mobile;
    private String no; 
    private String village; 
    private String moo;
    private String soi; 
    private String road; 
    private String provinceId; 
    private String amphurId;
    private String tambonId;
    private String postCode; 
    private String verifiedType;
    private String captcha;
    private String agreement;
    private String password;
    private String confirmPassword;
    private Address billingAddress;
    private Address registerAddress;
}