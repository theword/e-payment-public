package com.orange.epayment.endpoint.response;

import com.wisesoft.epayment.domain.Citizen;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CorporateResponse extends Citizen {
    private AddressResponse billingAddress;
    private AddressResponse workingAddress;
}