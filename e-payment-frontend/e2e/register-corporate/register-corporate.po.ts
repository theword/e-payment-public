import { browser, by, element, promise } from 'protractor';

export class RegisterCorporatePage {
    navigateTo() {
        return browser.get('/member/register-corporate');
    }

    getResultText(): promise.Promise<string> {
        return element(by.id('name')).getText();
    }

    setId(id) {
        element(by.xpath('//input[@formcontrolname="id"]')).sendKeys(id);
    }

    submit() {
        element(by.buttonText('Submit')).click();
    }
}
