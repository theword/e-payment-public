import { Component, OnInit, ViewChild } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import * as toastr from 'toastr';
import { ActivatedRoute } from '@angular/router';
import { DownloadTableComponent } from '../common/download/download-table.component';

@Component({
    selector: 'app-download-generic',
    templateUrl: 'download-generic.component.html',
})

export class DownloadGenericComponent implements OnInit {
    title: any;
    type: any;


    @ViewChild(DownloadTableComponent) table: DownloadTableComponent;

    constructor(private route: ActivatedRoute) {

    }

    ngOnInit() {
        this.route.queryParamMap.subscribe(q => {
            this.title = q.get('title');
        });
        this.route.paramMap.subscribe(q => {
            this.type = q.get('type');
            this.table.type = this.type;
            this.table.load();
        });
    }
}