package com.orange.epayment.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Post extends AbstractEntity {

	private Long author;

	@Column(length = 5000)
	private String title;

	@Column(length = 1000000)
	private String content;

	private Date createdDate;

	private Date modifiedDate;

	private String mime;

	@Enumerated(EnumType.STRING)
	private PostType type;

	@Enumerated(EnumType.ORDINAL)
	private PostStatus status;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "post")
	private List<PostMeta> meta;

}