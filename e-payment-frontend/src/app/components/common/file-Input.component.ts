import { Component, OnInit, forwardRef, AfterViewInit, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { DialogComponent } from './app-dialog.component';

@Component({
    selector: 'app-file-input',
    template: `
        <input type="file" #file (change)="onFileChange($event)" [accept]="accept">
        
        <button class="btn btn-primary btn-block btn-filebrowser" (click)="browseFile()">{{fileName}}</button>

        <app-dialog title="เกิดข้อผิดพลาด" #FileSizeExceedDialog>
            <p>
            {{sizeExceedMessage}}
            </p>
        </app-dialog>
    `,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FileInputComponent),
            multi: true
        }
    ]
})
export class FileInputComponent implements AfterViewInit, ControlValueAccessor {

    @Input() maxMB: number;
    @Input() sizeExceedMessage: string;
    @Input() accept: string;
    @ViewChild('FileSizeExceedDialog') fileSizeExceedDialog: DialogComponent;
    _value = '';
    onChange: any = () => { };
    onTouched: any = () => { };
    @ViewChild('file') fileBrowserInput;

    fileName = '';



    constructor() { }

    ngAfterViewInit() {

    }

    browseFile() {
        this.fileBrowserInput.nativeElement.click();
    }

    onFileChange(event) {
        let max = this.maxMB;
        if (!max)
            max = 50;

        const reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            const file = event.target.files[0];
            if (file.size > (1024 * 1024 * max)) {
                this.fileSizeExceedDialog.open();
                event.target.value = '';
                this.value = null;
                return;
            }

            const name = file.name;
            let extension = name.substr((name.lastIndexOf('.')));
            let check = this.accept.split(',');
            let found = check.find(c => c.trim() === extension);

            if (!found) {
                this.fileSizeExceedDialog.open();
                event.target.value = '';
                this.value = null;
                return;
            }

            reader.readAsDataURL(file);
            reader.onload = () => {
                this.fileName = event.target.files[0].name;
                this.value = event.target.files[0].name + ';' + reader.result.split(',')[1];
            };
        } else {
            this.value = null;
        }
    }

    get value() {
        return this._value;
    }

    set value(val: any) {
        if (!val)
            this.fileName = '';

        this._value = val;
        this.onChange(val);
        this.onTouched();
    }

    registerOnChange(fn) {
        this.onChange = fn;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }

    writeValue(value) {
        if (value) {
            this.value = value;
        }
    }
}
