import { Component, OnInit, ViewChild, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as simplecaptcha from 'angular-captcha';
import { ElementRef } from '@angular/core';
import { NgModel } from '@angular/forms';
import { environment } from '../../../environments/environment';
@Component({
    selector: 'app-captcha',
    template: `
    <div class="frm_col half-n-half" >
                <label>กรุณากรอกสิ่งที่ท่านเห็น <small class="red">*</small></label>
                <input
                type="text"
                id="captchaCode"
                name="captchaCode"
                #captchaCode="ngModel"
                ngModel
                correctCaptcha
              >
    </div>
    <div class="frm_col half-n-half">
    <botdetect-captcha styleName="epayment"></botdetect-captcha>
    <div class="alert-danger"
  *ngIf="captchaCode.errors?.incorrectCaptcha && !captchaCode.pristine"
  >
  กรอกสิ่งที่เห็นไม่ถูกต้อง, กรุณาลองใหม่อีกครั้ง
</div>
    </div>
    `,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => CaptchaComponent),
            multi: true
        }
    ]
})

export class CaptchaComponent implements OnInit, ControlValueAccessor {

    _value = '';


    @ViewChild('captchaCode') code: NgModel;
    @ViewChild(simplecaptcha.CaptchaComponent) captchaComponent: simplecaptcha.CaptchaComponent;
    onChange: any = () => { };
    onTouched: any = () => { };

    constructor() { }

    ngOnInit() {
        this.code.statusChanges.subscribe(v => {
            if (v === 'VALID') {
                this.writeValue(this.code.value);
            } else {
                if (!environment.production)
                    this.value = 'development';
                else
                    this.value = '';
            }
        });
    }



    resolved($event) {
        this.value = $event;
    }

    get value() {
        return this._value;
    }

    set value(val: any) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
    }

    registerOnChange(fn) {
        this.onChange = fn;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }

    writeValue(value) {
        if (value) {
            this.value = value;
        }
    }

    reset() {
        this.value = '';
        this.captchaComponent.reloadImage();
    }
}
