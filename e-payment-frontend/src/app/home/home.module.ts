import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { ServicesComponent } from './services.component';
import { BannerComponent } from './banner.component';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '../components/common/app-common.module';

const routes: Routes = [
    { path: '', component: HomeComponent }
];

@NgModule({
    imports: [
        AppCommonModule,
        CommonModule,
        RouterModule.forChild(routes)],
    exports: [],
    declarations: [
        HomeComponent,
        ServicesComponent,
        BannerComponent],
    providers: [],
})
export class HomeModule { }
