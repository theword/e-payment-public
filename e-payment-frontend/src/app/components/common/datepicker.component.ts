import { Directive } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import * as moment from 'moment';
import { ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
    selector: 'app-datepicker',
    template: `
        <input id="bdatepicker" type="text" value="" [placeholder]="placeholder" [readonly]="disabled">
    `,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DatePickerComponent),
            multi: true
        }
    ]

})
export class DatePickerComponent implements AfterViewInit, ControlValueAccessor {


    format: any = 'dd/mm/yyyy';
    @Input() valueFormat: string;
    @Input() placeholder: any;
    @Input() disabled: boolean;

    _date = '';
    _value = '';

    onChange: any = () => { };
    onTouched: any = () => { };

    constructor(private el: ElementRef) {

    }

    ngAfterViewInit() {
        const comp = this;
        const today = moment();

        jQuery(this.el.nativeElement).datepicker({
            autoclose: true,
            todayHighlight: true,
            format: this.format,
            endDate: today.toDate(),
            language: 'th',
            enableOnReadonly: false,
            thaiyear: true,
            todayBtn: true,
            defaultViewDate: { year: today.year(), month: today.month(), day: today.date() }
        }).on('changeDate', function (e) {
            const date = moment(e.date);
            comp.parseDate(date, comp);
        });

    }

    parseDate(date: any, comp: DatePickerComponent) {
        if (comp.valueFormat !== undefined) {
            comp.value = moment(date).format(comp.valueFormat.toUpperCase());
        } else {
            comp.value = date.toDate();
        }
    }

    get value() {
        return this._value;
    }

    set value(val: any) {

        this._value = val;
        this.onChange(val);
        this.onTouched();

        const date = moment(+val);
        console.log(date);
        jQuery(this.el.nativeElement).datepicker('update', date.toDate());

    }

    registerOnChange(fn) {
        this.onChange = fn;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }

    writeValue(value) {
        if (value) {
            console.log(value);
            const date = moment(parseInt(value, 10)).add(543, 'year');
            jQuery(this.el.nativeElement).datepicker('setDate', date.toDate());
            this.value = value;
        }
    }

}

