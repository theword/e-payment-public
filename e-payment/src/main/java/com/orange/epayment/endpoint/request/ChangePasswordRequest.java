package com.orange.epayment.endpoint.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePasswordRequest {
    private String currentpassword;
    private String password;
    private String confirmPassword;
    private String captcha;
}